
{-|
  Module      : Pretty
  Description : Pretty-Printing for Hart Data Structures.
  Copyright   : (c) Simon Désaulniers, 2021
  License     : GPL-3

  Maintainer  : sim.desaulniers@gmail.com

  This modules exposes functions for pretty printing Hart's data structures.
  This is used for debugging purposes only.
-}

module Hart.Pretty where

import Data.Tree
import Data.Tree.Pretty

import Hart.Internal.Data.LBSTree

fromBSTree :: Show a => BSTree a -> Tree String
fromBSTree (Leaf n) = Node (show n) []
fromBSTree (Branch b0 n b1) = Node (show n) $ map fromBSTree [b0, b1]

ppBSTree :: Show a => BSTree a -> String
ppBSTree = drawVerticalTree . fromBSTree

--  vim: set sts=2 ts=2 sw=2 tw=120 et :

