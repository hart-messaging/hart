
## Hart

A Haskell implementation of the ART end-to-end secure group messaging protocol.

ART is an acronym for Asynchronous Ratcheting Tree. This protocol was first
published in a preprint paper hosted on
https://eprint.iacr.org/2017/666/20170705:223414. The latest (to this day) is
found [here](https://eprint.iacr.org/2017/666/20200302:221723). Note that the
paper was also published in the [proceedings of the 2018 ACM SIGSAC Conference on
Computer and Communications Security][ACMSIGSACpub].

The project is based on the reading of the latest version of the paper to this
day and other conversations found on the <messaging@moderncrypto.org>
mailing list.

### Motivation

The writing of Hart is highly motivated by the following:

* Providing a more complete and functional library for real use and not limited
  to an implementation written for the sake of minimally supporting research
  work credibility;
* A desire for a more deep understanding of the implementation details of the
  protocol;
* A pretext for updating my knowledege about the protocol, the alternatives and
  new techniques for solving open questions in the field of E2E secure group
  messaging protocols in the group setting.

### Other implementation

There is an [existing java implementation][javaimplem] by the authors of the paper. This
project may be useful in comparing implementation details.

### Messaging Layer Security (MLS)

The [MLS working group][mls], started in 2018, is focused on building on top
of ideas of ART. The group has produced a specification that completes ART with
several concerns left "out-of-scope" by the authors of ART. There other
implementations of those ideas listed [here][mls-implementations].

While Hart is an implementation of ART, in order motivate its seriousness, it
needs to provide answers for the concerns left out-of-scope by ART. Therefore,
it might take ideas from MLS in the future.

### Dependencies

The project is being developped using GHC version 8.10.7. Therefore, it is
advised to stick with this version.

The build system is based on [Cabal][]. The version used at the moment is 2.2.
Using a version greater or equal should be fine.

Haskell package dependencies are listed in the file `hart.cabal`. These are
automatically installed by cabal locally.

[Cabal]: https://www.haskell.org/cabal/

### Documentation

You can generate documentation of the haskell modules by running the following:

```
$ cabal new-haddock hart
```

This will generate a `index.html` file you can visit under the following
directory:

```
dist-newstyle/build/${ARCH}-${OS}/ghc-${GHCVERSION}/hart-${PROJECTVERSION}/doc/html/hart
```

### Author(s)

Simon Désaulniers (<sim.desaulniers@gmail.com>)

[mls]: https://messaginglayersecurity.rocks/
[mls-implementations]: https://github.com/mlswg/mls-implementations/blob/master/implementation_list.md
[ACMSIGSACpub]: https://people.cispa.io/cas.cremers/downloads/papers/CCGMM2018-groupmessaging.pdf
[javaimplem]: https://github.com/facebookresearch/asynchronousratchetingtree

<!-- vim: set sts=2 ts=2 sw=2 tw=80 et :-->

