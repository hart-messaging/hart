
# Docker hub destination: sim590/hart

FROM haskell:8.6.5
MAINTAINER Simon Désaulniers <sim.desaulniers@gmail.com>

RUN mkdir -p /opt/hart
WORKDIR /opt/hart
COPY hart.cabal .
RUN cabal new-update && cabal new-build -j4 --only-dependencies --enable-tests \
                     && cabal new-install -j4 --lib --only-dependencies

WORKDIR /opt/
RUN rm -rf hart

#  vim: set ft=dockerfile ts=4 sw=4 tw=120 et :

