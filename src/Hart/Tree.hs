{-|
  Module      : Hart.Tree
  Description : Bindings for ART Tree.
  Copyright   : (c) Simon Désaulniers, 2020
  License     : GPL-3

  Maintainer  : sim.desaulniers@gmail.com

  This modules provides functions for working with the binary tree structure as
  described by the [ART
  protocol](https://people.cispa.io/cas.cremers/downloads/papers/CCGMM2018-groupmessaging.pdf).
  For associated data types, see module "Hart.Data.Tree".

  All of the functions in this module run in a monad that is capable of handling
  logging. That is enforced by the usage of `WithLog`, from the module
  "Colog.Monad", as a class constraint on `m`. You can either provide your own
  logging plumbing or use the default provided in "Hart.Log".

  For running these functions, the user of this library is encouraged to look at
  "Hart.Monad" for the monad transformer `Hart.Monad.HartTreeT` which will
  eliminate a significant amount of boilerplate code. This transformer does work
  in conjunction with "Hart.Log".
-}

{-# LANGUAGE FlexibleContexts #-}

module Hart.Tree ( users
                 , rootKey
                 , copath
                 , initializeLocally
                 , initializeFromRemoteData
                 , addUser
                 , removeUser
                 , updateKey
                 , updateLocalUserPrivateKey
                 ) where

import Data.Maybe

import Control.Lens.Extras
import Control.Applicative
import Control.Monad
import Control.Monad.State
import Control.Monad.Trans.Maybe

import Control.Monad.Chronicle

import GHC.Stack

import Colog.Core
import Colog.Message
import Colog.Monad

---------------------
--  Local modules  --
---------------------

import Hart.Internal.Log

import Hart.DiffieHellman
import Hart.Internal.MChronicle
import Hart.Data.Tree
import Hart.Internal.Data.LBSTree (LBSTree)
import qualified Hart.Internal.Tree as IT

maybeEmptyReturnFromMaybeChronicle :: WithLog env Message m =>
  MaybeT (ChronicleT Messages m) a
  -> MaybeT m a
maybeEmptyReturnFromMaybeChronicle = maybe empty return <=< runChronicleWithLog . runMaybeT

validateBSTLocalUser :: MonadChronicle Messages f => LBSTree (ArtNode v b) -> f ()
validateBSTLocalUser bst = unless (isJust $ IT.getLocalUser bst)
                           $ confess [Msg Error callStack IT._MISSING_LOCAL_USER_IN_ARTTREE_]

{-| Fetches the list of user information for each user in the `ArtTree`.

   When an unexpected value is passed, an error is logged and the default value
   `[]` is returned.
-}
users :: WithLog env Message m => ArtTree v b -> m [UserInfo v b]
users = return . fromMaybe [] <=< runMaybeT . runChronicleWithLog . hoist' . IT.users

{-| Yields the root private key of the `ArtTree`.

   When an unexpected is passed, an error is logged and `Nothing` is returned.
-}
rootKey :: WithLog env Message m => ArtTree v b -> MaybeT m v
rootKey t@ArtTree{treeData = bst} = runChronicleWithLog $ hoist' $ do
  validateBSTLocalUser bst
  IT.rootKey t

{-| Computes the copath of a node in the ART tree structure based on a given
   user ID.

   If the user ID is not found, `empty` is returned. Otherwise, the copath is
   returned.

   If there are any unexpected errors, during execution, then an error is logged
   and `empty` is returned.
-}
copath :: (WithLog env Message m, Eq v, Eq b)
  => UserID      -- ^ The user ID of the user to look for.
  -> ArtTree v b -- ^ The ART tree structure.
  -> MaybeT m [ArtNode v b]
copath uid = maybeEmptyReturnFromMaybeChronicle . IT.copath uid


{-| Initializes an ART tree data structure as the initiator of the group.

   When an empty list is passed, `empty` is returned.
-}
initializeLocally :: ( WithLog env Message m
                     , MonadState (DHMaterial z) m
                     , Eq b
                     , Eq v
                     , DHComputable z v b
                     )
  => [(UserID, UserInfo v b)] -- ^ List of user IDs and associated user info.
  -> MaybeT m (ArtTree v b)
initializeLocally id_info_pairs = maybeEmptyReturnFromMaybeChronicle $ do
  let
    has_local_user  = any (is _LocalUser . snd) id_info_pairs
    su_remote_users = filter (is _StartUpRemoteUser . snd) id_info_pairs

  unless (has_local_user && length su_remote_users == length id_info_pairs - 1) $
    lift $ confess [Msg Error callStack IT._MISSING_ARTTREE_INITIALIZATION_DATA_]

  IT.initializeLocally id_info_pairs


{-| Initializes an ART tree data structure from data received from another peer.

   An empty list as input will lead to the return of `empty` without errors.

   When an unexpected value is passed, an error is logged and `empty` is returned.
-}
initializeFromRemoteData
  :: (WithLog env Message m, MonadState (DHMaterial z) m, Eq b, Eq v, DHComputable z v b)
  => [(UserID, UserInfo v b)] -- ^ List of user IDs and associated user info.
  -> [b]                      -- ^ The list of public keys found on the copath of the user in
                              --   bottom-up order.
  -> MaybeT m (ArtTree v b)
initializeFromRemoteData id_info_pairs pks = maybeEmptyReturnFromMaybeChronicle $ do
  let
    has_local_user = any (is _LocalUser . snd) id_info_pairs
    remote_users   = filter (is _RemoteUser . snd) id_info_pairs

  unless (has_local_user && length remote_users == length id_info_pairs - 1) $
    lift $ confess [Msg Error callStack IT._MISSING_ARTTREE_INITIALIZATION_DATA_]

  IT.initializeFromRemoteData id_info_pairs pks


{-| Adds a user to an ArtTree.

   When an unexpected value is passed, an error is logged and `empty` is returned.
-}
addUser :: (WithLog env Message m, MonadState (DHMaterial z) m, Eq b, Eq v, DHComputable z v b) =>
  UserID           -- ^ The user ID of the new user.
  -> UserInfo v b  -- ^ The user info of the new user.
  -> b             -- ^ The public key replacing a key on our copath after
                   --   adding this new user.
  -> ArtTree v b   -- ^ The ART tree structure.
  -> MaybeT m (ArtTree v b)
addUser uid uinfo pk t@ArtTree{treeData = bst} = runChronicleWithLog $ do
  validateBSTLocalUser bst
  IT.addUser uid uinfo pk t


{-| Removes a user from an ArtTree.

   When no user remain in the tree, `empty` is returned without error.

   When only one user remain in the tree, the key passed as second argument is simply ignored.

   When an unexpected value is passed, an error is logged and `empty` is returned.
-}
removeUser :: (WithLog env Message m, MonadState (DHMaterial z) m, DHComputable z v b, Eq v, Eq b) =>
  UserID         -- ^ The user ID of the user to remove.
  -> b           -- ^ The public key replacing a key on our copath by removing the user.
  -> ArtTree v b -- ^ The ART tree structure.
  -> MaybeT m (ArtTree v b)
removeUser uid pk t@ArtTree{treeData = bst} = maybeEmptyReturnFromMaybeChronicle $ do
  validateBSTLocalUser bst
  IT.removeUser uid pk t


{-| Updates a user's public key.

   When an unexpected value is passed, an error is logged and `empty` is returned.
-}
updateKey :: (WithLog env Message m, MonadState (DHMaterial z) m, Eq b, Eq v, DHComputable z v b) =>
  UserID         -- ^ The user id belonging to the user which we update the key
  -> b           -- ^ The user's new public key
  -> b           -- ^ The public key modified on our copath by updating the user
  -> ArtTree v b -- ^ The ART tree structure
  -> MaybeT m (ArtTree v b)
updateKey uid pk cpPk t@ArtTree{treeData = bst} = runChronicleWithLog $ do
  validateBSTLocalUser bst
  IT.updateKey uid pk cpPk t


{-| Updates the local user's private key.

   When an unexpected value is passed, an error is logged and `empty` is returned.
-}
updateLocalUserPrivateKey :: (MonadState (DHMaterial z) m, DHComputable z v b) =>
     v           -- ^ The local user's new private key
  -> ArtTree v b -- ^ The ART tree structure
  -> ChronicleT Messages m (ArtTree v b)
updateLocalUserPrivateKey vk art_tree@ArtTree{treeData = bst} = do
  validateBSTLocalUser bst
  IT.updateLocalUserPrivateKey vk art_tree

--  vim: set sts=2 ts=2 sw=2 tw=120 et :

