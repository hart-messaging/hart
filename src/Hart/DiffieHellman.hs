{-|
  Module      : DiffieHellman
  Description : Diffie-Hellman definitions.
  Copyright   : (c) Simon Désaulniers, 2021
  License     : GPL-3

  Maintainer  : sim.desaulniers@gmail.com

  This module exposes types and classes which translate assumptions necessary to
  manipulate Diffie-Hellman operations such as the well-known Diffie-Hellman
  exhchange.
-}

{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FunctionalDependencies #-}

module Hart.DiffieHellman where

import Control.Lens
import Control.Monad.State

newtype DHMaterial z = DHMaterial { _generator :: z
                                  }
makeLenses ''DHMaterial

{-| Class of operations on public and private DH keys.
-}
class DHComputable z v b | z v -> b where
  {-| Performs a Diffie-Hellman exchange between a private and public key and
     yields a result ready to populate a private key field.
  -}
  dhExchange :: MonadState (DHMaterial z) m
             => v -- ^ The private key
             -> b -- ^ The public key
             -> m v
  {-| Creates a public key from a private key.
  -}
  exponentiate :: MonadState (DHMaterial z) m => v -> m b


--  vim: set sts=2 ts=2 sw=2 tw=120 et :

