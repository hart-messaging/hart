
{-|
  Module      : Hart.Log
  Description : Logging utility bindings.
  Copyright   : (c) Simon Désaulniers, 2020
  License     : GPL-3

  Maintainer  : sim.desaulniers@gmail.com

  This module contains logging related code of Hart. It defines default
  logging formats and functions.

  To disable logging, one can simply use `mempty` as the `LogAction` argument
  provided to `usingLoggerT` which is exactly what `noLogging` does.

  The formatting and IO functions provided in this module exist only to make
  life easier for the user of this library. Indeed, it is perfectly doable to
  make your own plumbing of functions as you like.

  You may use either `toStdOut`, `toStdErr` or `noLogging` if you're not using
  the boilerplate code in `Hart.Monad`. Otherwise, you'll prefer to use
  HartLogAction based functions.
-}

{-# LANGUAGE ScopedTypeVariables #-}

module Hart.Log where

import System.IO

import Control.Monad.Reader
import Control.Monad.Identity

import Colog.Core
import Colog.Actions
import Colog.Monad
import Colog.Message

import Hart.Monad

---------------------------------------------
--  Default Colog formatting combinations  --
---------------------------------------------

{-| This runs a function ignoring logged messages.
-}
noLogging :: LoggerT Message Identity a -> a
noLogging = runIdentity . usingLoggerT mempty

{-| Log a Message to `stdout`.

  For instance, to log messages to `stdout` when calling the `users` function
  from the "Hart.Tree" module, then use it like:

  @usingLoggerT toStdOut users@
-}
toStdOut :: LogAction IO Message
toStdOut = formatWith fmtMessage logTextStdout

{-| Log a Message to `stderr`.
-}
toStdErr :: LogAction IO Message
toStdErr = formatWith fmtMessage logTextStderr

{-| Log a Message to `stderr`.

  For instance, to log messages to some `Handle` when calling the `users` function
  from the "Hart.Tree" module, then use it like:

  @usingLoggerT (toHandle h) users@

  where `h` is a `Handle`.
-}
toHandle :: Handle -> LogAction IO Message
toHandle = formatWith fmtMessage . logTextHandle

{-| Create a HartLogAction based on a function specifying how to handle
   messages in a MonadIO environment.
-}
makeHartLogAction :: forall m z.  MonadIO m =>
  (Message -> m ()) -- ^ Function handling a `Message` in a MonadIO environment.
  -> HartLogAction m z Message
makeHartLogAction ioMessageHandler = HartLogAction $ LogAction messageHandler
  where
    messageHandler :: Message -> HartTreeT z m ()
    messageHandler = lift . ioMessageHandler

{-| `noLogging` wrapped in HartLogAction.

  @runHartTreeT (runMaybeT $ Hart.Tree.initializeLocally userIdInfoPairs) hartNoLogging (DHMaterial (0 :: Int))@
-}
hartNoLogging :: HartLogAction IO z Message
hartNoLogging = makeHartLogAction . unLogAction $ getLogAction (mempty :: LogAction IO Message)

{-| `toStdErr` wrapped in HartLogAction.
-}
hartToStdErr :: HartLogAction IO z Message
hartToStdErr = makeHartLogAction . unLogAction $ getLogAction toStdErr

{-| `toHandle` wrapped in HartLogAction.
-}
hartToHandle :: Handle -> HartLogAction IO z Message
hartToHandle = makeHartLogAction . unLogAction . getLogAction . toHandle

--  vim: set sts=2 ts=2 sw=2 tw=120 et :

