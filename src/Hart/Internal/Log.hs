
{-|
  Module      : Hart.Internal.Log
  Description : Internal bindings for logging.
  Copyright   : (c) Simon Désaulniers, 2020
  License     : GPL-3

  Maintainer  : sim.desaulniers@gmail.com

  This module contains internal utility definitions for logging.
-}

{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE FlexibleContexts #-}

module Hart.Internal.Log where

import Data.These
import Data.These.Combinators

import Control.Monad
import Control.Applicative
import Control.Monad.Trans
import Control.Monad.Trans.Maybe
import Control.Monad.Chronicle

import Colog.Monad
import Colog.Message

import Hart.Log

type Messages = [Message]

{-| Run `Chronicle` action and ignore messages.
-}
runChronicleWithoutLog :: MonadIO m => ChronicleT Messages m a -> MaybeT m a
runChronicleWithoutLog = MaybeT . return . justThere <=< lift . runChronicleT

{-| Runs a `Chronicle` action and log returned messages before returning the
   result.
-}
runChronicleWithLog :: WithLog env Message m => ChronicleT Messages m a -> MaybeT m a
runChronicleWithLog = lift . runChronicleT >=> \ case
    This msgs    -> lift (logMsgs msgs) >> empty
    That r       -> return r
    These msgs r -> lift (logMsgs msgs) >> return r

{-| Runs a `Chronicle` action and log returned messages to `stderr` before
   returning the result.
-}
runChronicleWithLogToStdErr :: ChronicleT Messages (LoggerT Message IO) a -> MaybeT IO a
runChronicleWithLogToStdErr = MaybeT . usingLoggerT toStdErr . runMaybeT . runChronicleWithLog

--  vim: set sts=2 ts=2 sw=2 tw=120 et :

