
{-|
  Module      : Hart.Internal.Tree
  Description : Internal bindings relative to "Hart.Tree".
  Copyright   : (c) Simon Désaulniers, 2020
  License     : GPL-3

  Maintainer  : sim.desaulniers@gmail.com

  This module is the internal counterpart of "Hart.Tree". It contains actual
  implementations of its API. For documentation of every function, see the
  actual exposed API of the aforementioned module.
-}

{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}

module Hart.Internal.Tree where

import Data.List
import Data.Maybe
import Data.Text (Text)

import qualified Data.Set as Set

import GHC.Stack

import Colog.Core
import Colog.Message

import Control.Lens
import Control.Lens.Extras
import Control.Applicative
import Control.Monad.Trans.Class
import Control.Monad.Trans.Maybe
import Control.Monad.Chronicle
import Control.Monad.State

---------------------
--  Local modules  --
---------------------

import Hart.DiffieHellman
import Hart.Data.Tree

import Hart.Internal.Log
import Hart.Internal.Data.LBSTree ( LBSNode (..)
                                  , LBSTree
                                  )
import qualified Hart.Internal.Data.LBSTree as LBST

----------------
--  Bindings  --
----------------

_MISSING_ARTTREE_INITIALIZATION_DATA_ :: Text
_MISSING_ARTTREE_INITIALIZATION_DATA_ = "Initialization needs a single local user info and \
                                        \startup remote user material."

_ILLFORMED_ARTTREE_LEAF_ :: Text
_ILLFORMED_ARTTREE_LEAF_ = "ArtTree has an ill-formed leaf."

_MISSING_ROOT_PRIVATE_KEY_ :: Text
_MISSING_ROOT_PRIVATE_KEY_ = "The ArtTree root is missing a private key."

_MISSING_LOCAL_USER_IN_ARTTREE_ :: Text
_MISSING_LOCAL_USER_IN_ARTTREE_ = "No local user info found in the tree."

_UNKNOWN_USER_ID_ :: Text
_UNKNOWN_USER_ID_ = "A user ID that could not be found was passed to this function."

{-| Retrieves an ArtNode private key.
-}
nodePrivateKey :: ArtNode v b -> Maybe v
nodePrivateKey = either (view internalNodePrivateKey) (preview userPrivateKey)

{-| Retrieves an ArtNode public key.
-}
nodePublicKey :: ArtNode v b -> Maybe b
nodePublicKey = either (view internalNodePublicKey) (Just . view userPublicKey)

{-| Assigns a public key to an ArtNode.
-}
setNodePublicKey :: b -> ArtNode v b -> ArtNode v b
setNodePublicKey pk = bimap (& internalNodePublicKey ?~ pk) (& userPublicKey .~ pk)

setNodePrivateKey :: v -> ArtNode v b -> ArtNode v b
setNodePrivateKey vk = bimap (& internalNodePrivateKey ?~ vk) (& userPrivateKey .~ vk)

{-| Yields a pair consisting respectively of a private and a public key taken
   from data found in the left and right node.
-}
keysFromSiblings ::
  ArtNode v b    -- ^ The left node.
  -> ArtNode v b -- ^ The right node.
  -> Maybe (v, b)
keysFromSiblings n0 n1 = do
  let mvk0 = nodePrivateKey n0
      mvk1 = nodePrivateKey n1
      mpk0 = nodePublicKey n0
      mpk1 = nodePublicKey n1

  let jvks       = filter (uncurry (&&) . bimap isJust isJust) [(mvk0, mpk1), (mvk1, mpk0)]
      (mvk, mpk) = head jvks

  guard $ not (null jvks)
  return (fromJust mvk, fromJust mpk)

{-| Computes the parent node from its children. A private key is taken from
   either the left or the right child. The public counter part is taken from the
   other node and computation occurs using the DHComputable z implementation.

   __NOTE__: Signature now lives in ChronicleT Messages m, but doesn't need it.
   While, it is not yet clear if at some point, we'd need it, we keep it like
   this.
-}
computeParent :: (MonadState (DHMaterial z) m, DHComputable z v b) =>
  ArtNode v b    -- ^ The left node.
  -> ArtNode v b -- ^ The right node.
  -> ChronicleT Messages m (ArtNode v b)
computeParent n0 n1 = do
  let
    mkeys = keysFromSiblings n0 n1
    compute (vk, pk) = do
      parent_vk <- lift $ dhExchange vk pk
      parent_pk <- lift $ exponentiate parent_vk
      return (Just parent_vk, Just parent_pk)
  (mparent_vk, mparent_pk) <- maybe (return (empty, empty)) compute mkeys
  return $ Left $ InternalNodeInfo mparent_vk mparent_pk

{-| Inserts a list of public keys along the copath of some node at a given index
   in the tree.
-}
insertCopath :: (MonadState (DHMaterial z) m, DHComputable z v b) =>
  Int                      -- ^ The index of the node which the copath is related to.
  -> [b]                   -- ^ The list of public keys to assign on the copath.
  -> LBSTree (ArtNode v b) -- ^ The tree containing the user data.
  -> MaybeT (ChronicleT Messages m) (LBSTree (ArtNode v b))
insertCopath i pks bst = lift $ LBST.cpMapAccum computeParent insert_pks i pks bst
  where
    insert_pks [] n             = ([], n)
    insert_pks (pk:pks') node   = (pks', setNodePublicKey pk node)

{-| Retrieves the local node. Only one should be present in the tree.
-}
getLocalUser :: LBSTree (ArtNode v b) -> Maybe (LBSNode (ArtNode v b))
getLocalUser bst = find (maybe False (is _LocalUser) . preview (LBST.content . _Right)) usrs
  where usrs = LBST.leaves bst

{-| Computes the copath of a node in the ART tree structure based on a given
   user ID.

   Note: This function doesn't validate its input (unless forced to). Validation
   is done at API level.

   See Hart.Tree for complete description.
-}
copath :: (HasCallStack, Monad m, Eq v, Eq b) =>
  UserID         -- ^ The user ID of the user to look for.
  -> ArtTree v b -- ^ The ART tree structure.
  -> MaybeT (ChronicleT Messages m) [ArtNode v b]
copath uid ArtTree{treeData = bst, treeUIDs = uids} = do
  let mi = Set.lookupIndex uid uids
  unless (isJust mi) $ dictate [Msg Warning callStack _UNKNOWN_USER_ID_]
  i <- MaybeT $ return mi
  n <- lift $ LBST.get i bst
  lift $ map (view LBST.content) <$> LBST.copath n bst

{-| Retrieves the list of users stored in the tree.

   Note: This function doesn't validate its input (unless forced to). Validation
   is done at API level.

   See Hart.Tree for details.
-}
users :: HasCallStack => ArtTree v b -> Chronicle Messages [UserInfo v b]
users = mapM getInfo . LBST.leaves . treeData
  where getInfo :: LBSNode (ArtNode v b) -> Chronicle Messages (UserInfo v b)
        getInfo (LBSLeaf _ (Right info')) = return info'
        getInfo _                         = confess [Msg Error callStack _ILLFORMED_ARTTREE_LEAF_]

{-| Yields the tree root private key.

   See Hart.Tree for complete description.
-}
rootKey :: HasCallStack => ArtTree v b -> Chronicle Messages v
rootKey tree = do
  let mvk = nodePrivateKey $ view LBST.content $ LBST.root $ treeData tree
  when (isNothing mvk) $ confess [Msg Error callStack _MISSING_ROOT_PRIVATE_KEY_]
  return $ fromJust mvk

{-| Initializes an ART tree data structure as the initiator of the group.

   Note: This function doesn't validate its input (unless forced to). Validation
   is done at API level.

   See Hart.Tree for complete description.
-}
initializeLocally :: (HasCallStack, MonadState (DHMaterial z) m, Eq b, Eq v, DHComputable z v b) =>
  [(UserID, UserInfo v b)] -- ^ List of user IDs and associated user info.
  -> MaybeT (ChronicleT Messages m) (ArtTree v b)
initializeLocally id_info_pairs = do
  let
    uids           = Set.fromList $ map fst id_info_pairs
    sorted_u_infos = map snd $ sortBy (\ (id0, _) (id1, _) -> compare id0 id1) id_info_pairs
    art_leaves     = map Right sorted_u_infos

  bst <- LBST.fromList computeParent art_leaves
  return $ ArtTree bst uids

{-| Initializes an ART tree data structure from data received from another peer.

   Note: This function doesn't validate its input (unless forced to). Validation
   is done at API level.

   See Hart.Tree for complete description.
-}
initializeFromRemoteData
  :: (HasCallStack, MonadState (DHMaterial z) m, Eq b, Eq v, DHComputable z v b)
  => [(UserID, UserInfo v b)] -- ^ List of user IDs and associated user info.
  -> [b]                      -- ^ The list of public keys found on the copath of the user in
                              --   bottom-up order.
  -> MaybeT (ChronicleT Messages m) (ArtTree v b)
initializeFromRemoteData id_info_pairs pks = do
  let
    m_local_user_id_info = find (is _LocalUser . snd) id_info_pairs
    uids                 = Set.fromList $ map fst id_info_pairs
    (luid, _)            = fromJust m_local_user_id_info
    lnode_index          = Set.findIndex luid uids
    sorted_u_infos       = map snd $ sortBy (\ (id0, _) (id1, _) -> compare id0 id1) id_info_pairs
    art_leaves           = map Right sorted_u_infos

  bst                    <- LBST.fromList computeParent art_leaves
  m_bst_with_remote_data <- insertCopath lnode_index pks bst
  return $ ArtTree m_bst_with_remote_data uids

{-| Yields the common copath node between a pair of nodes.

   More precisely, let \(T\) a binary tree and \(n_0, n_1 \in T\). Also, let
   \(p_c\) the `copath` of \(n_0\) and \(p\) the path of \(n_1\). This function
   returns the only node \(x \in T\) which satisfies \(x \in p_c \cap p\).
-}
commonCopathNode :: (HasCallStack, MonadState (DHMaterial z) m, Eq b, Eq v) =>
  LBSNode (ArtNode v b)    -- ^ The local node
  -> LBSNode (ArtNode v b) -- ^ The other node
  -> LBSTree (ArtNode v b) -- ^ The LBSTree
  -> ChronicleT Messages m (LBSNode (ArtNode v b))
commonCopathNode lnode onode bst = do
  local_copath  <- LBST.copath lnode bst
  new_user_path <- LBST.path onode bst
  return $ head $ intersect local_copath new_user_path

{-| Updates the public key of the node found on both the local node's copath and
   another node's path.

   When adding or removing a node, a node on the local node's copath has to be
   updated since the subtree containing the (new or old) node has changed.
-}
updateCommonCopathNode :: (MonadState (DHMaterial z) m, Eq b, Eq v, DHComputable z v b) =>
  LBSNode (ArtNode v b)    -- ^ The copath node
  -> b                     -- ^ The public key replacing a key on our copath by this new user.
  -> LBSTree (ArtNode v b) -- ^ The LBSTree
  -> ChronicleT Messages m (LBSTree (ArtNode v b))
updateCommonCopathNode cp_node pk bst = do
  let
    local_user              = fromJust $ getLocalUser bst
    local_user_id           = fromJust $ preview LBST.index local_user
    the_copath_node_content = fromJust $ preview LBST.content cp_node
    replace_pk a n          = (a, mapOnCopathNode n)
    mapOnCopathNode n
      | n == the_copath_node_content = setNodePublicKey pk n
      | otherwise                    = n
  LBST.cpMapAccum computeParent replace_pk local_user_id Nothing bst

{-| Adds a user to an ArtTree.

   Note: This function doesn't validate its input (unless forced to). Validation
   is done at API level.

   See Hart.Tree for complete description.
-}
addUser :: (HasCallStack, MonadState (DHMaterial z) m, Eq b, Eq v, DHComputable z v b) =>
  UserID           -- ^ The user ID of the new user.
  -> UserInfo v b  -- ^ The user info of the new user.
  -> b             -- ^ The public key with which to replace on our copath after
                   --   adding this new user.
  -> ArtTree v b   -- ^ The ART tree structure.
  -> ChronicleT Messages m (ArtTree v b)
addUser uid uinfo pk ArtTree{treeData = bst, treeUIDs = uids} = do
  let newUIDs      = Set.insert uid uids
      i            = Set.findIndex uid newUIDs

  bst_with_new_user <- LBST.insert computeParent i (Right uinfo) bst

  let local_user = fromJust $ getLocalUser bst
      new_user   = LBSLeaf i $ Right uinfo
  cp_node   <- commonCopathNode local_user new_user bst_with_new_user
  final_bst <- updateCommonCopathNode cp_node pk bst_with_new_user
  return $ ArtTree final_bst newUIDs

{-| Removes a user from an ArtTree.

   Note: This function doesn't validate its input (unless forced to). Validation
   is done at API level.

   See Hart.Tree for complete description.
-}
removeUser :: (MonadState (DHMaterial z) m, DHComputable z v b, Eq v, Eq b) =>
  UserID         -- ^ The user ID of the user to remove.
  -> b           -- ^ The public key replacing a key on our copath by removing the user.
  -> ArtTree v b -- ^ The ART tree structure.
  -> MaybeT (ChronicleT Messages m) (ArtTree v b)
removeUser uid cp_pk ArtTree{treeData = bst, treeUIDs = uids} = do
  let
    mi         = Set.lookupIndex uid uids
    local_user = fromJust $ getLocalUser bst
  case mi of
    Just i -> do
      userToBeRemoved <- lift $ LBST.get i bst
      the_copath_node <- lift $ commonCopathNode local_user userToBeRemoved bst
      bst'            <- LBST.delete computeParent i bst
      bst''           <- lift $ updateCommonCopathNode the_copath_node cp_pk bst'
      let newUIDs = Set.delete uid uids
      return $ ArtTree bst'' newUIDs
    Nothing -> return $ ArtTree bst uids

{-| Updates a user's public key.

   Note: This function doesn't validate its input (unless forced to). Validation
   is done at API level.

   See Hart.Tree for complete description.
-}
updateKey :: (HasCallStack, MonadState (DHMaterial z) m, Eq b, Eq v, DHComputable z v b) =>
  UserID         -- ^ The user id belonging to the user which we update the key
  -> b           -- ^ The user new public key
  -> b           -- ^ The public key replacing a key on our copath by updating the user
  -> ArtTree v b -- ^ The ART tree structure
  -> ChronicleT Messages m (ArtTree v b)
updateKey uid pk cp_pk ArtTree{treeData = bst, treeUIDs = uids} = do
  let
    mi         = Set.lookupIndex uid uids
    local_user = fromJust $ getLocalUser bst
  case mi of
    Just i -> do
      user_to_update <- LBST.get i bst
      let
        u_art_node = view LBST.content user_to_update
        m_uinfo    = u_art_node ^? _Right
      unless (isJust m_uinfo) $ confess [Msg Error callStack _ILLFORMED_ARTTREE_LEAF_]
      let
        uinfo          = fromJust m_uinfo
        -- update on a StartUpRemoteUser makes it a remote user. Then the
        -- old private key is irrelevant.
        new_u_art_node = case uinfo of
          StartUpRemoteUser _ _ -> Right (RemoteUser pk)
          _                     -> setNodePublicKey pk u_art_node
      bst'            <- LBST.set computeParent new_u_art_node i bst
      updated_user    <- LBST.get i bst
      the_copath_node <- commonCopathNode local_user updated_user bst
      bst''           <- updateCommonCopathNode the_copath_node cp_pk bst'
      return $ ArtTree bst'' uids
    Nothing -> return $ ArtTree bst uids

{-| Updates the local user's private key.

   Note: This function doesn't validate its input (unless forced to). Validation
   is done at API level.

   See Hart.Tree for complete description.
-}
updateLocalUserPrivateKey :: (MonadState (DHMaterial z) m, DHComputable z v b) =>
     v           -- ^ The local user's new private key
  -> ArtTree v b -- ^ The ART tree structure
  -> ChronicleT Messages m (ArtTree v b)
updateLocalUserPrivateKey vk art_tree@ArtTree{treeData = bst} = do
  let
    local_user     = fromJust $ getLocalUser bst
    i              = fromJust $ preview LBST.index local_user
    u_art_node     = view LBST.content local_user
    new_u_art_node = setNodePrivateKey vk u_art_node
  bst' <- LBST.set computeParent new_u_art_node i bst
  return $ art_tree{treeData = bst'}

--  vim: set sts=2 ts=2 sw=2 tw=120 et :

