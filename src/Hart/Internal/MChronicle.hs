
module Hart.Internal.MChronicle where

import Control.Monad.Identity
import Control.Monad.Chronicle

hoist' :: Monad m => Chronicle c a -> ChronicleT c m a
hoist' = ChronicleT . pure . runIdentity . runChronicleT

--  vim: set sts=2 ts=2 sw=2 tw=120 et :

