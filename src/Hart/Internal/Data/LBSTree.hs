
{-|
  Module      : Hart.Internal.Data.LBSTree
  Description : Left balanced Binary Search Tree data type and bindings.
  Copyright   : (c) Simon Désaulniers, 2020
  License     : GPL-3

  Maintainer  : Simon Désaulniers

  This module exposes definition and bindings relevant to a Left balanced Binary
  Search Tree. This module is an extension of the "Hart.Internal.Data.BSTree"
  module. Specific insertion bindings are provided in order to keep the
  underlying `BSTree` structure left-balanced.

  When creating leaves in the tree, internal nodes are computed according to
  some function (with type `LBSPComputer`) passed by the caller.
-}

{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TemplateHaskell #-}

module Hart.Internal.Data.LBSTree ( module Hart.Internal.Data.BSTree
                                  , LBSNode (..)
                                  , _LBSInternalNode
                                  , _LBSLeaf
                                  , leftSubTreeSize
                                  , size
                                  , content
                                  , index
                                  , LBSTree
                                  , LBSPComputer
                                  , get
                                  , set
                                  , cpMapAccum
                                  , insert
                                  , fromList
                                  , delete
                                  , Hart.Internal.Data.LBSTree.init
                                  , Hart.Internal.Data.LBSTree.tail
                                  ) where

import Data.Maybe
import Data.Text (Text)

import Control.Lens (makeLenses, makePrisms, preview, view, over, _2)

import Control.Applicative
import Control.Monad
import Control.Monad.Trans
import Control.Monad.Trans.Maybe

import GHC.Stack

import Colog.Core
import Colog.Message

import Control.Monad.Chronicle

---------------------
--  Local modules  --
---------------------

import Hart.Internal.Log
import Hart.Internal.Data.BSTree

-------------
--  Types  --
-------------

data LBSNode a =
    LBSInternalNode
    { _size            :: Int -- ^ Size of the underlying list of leaves.
    , _leftSubTreeSize :: Int -- ^ Size of the left subtree's underlying list of leaves.
    , _content         :: a   -- ^ The content of the internal node.
    }
  | LBSLeaf
    { _index   :: Int  -- ^ The index of the leaf. This is the actual index
                       --   position in the list of leaves.
    , _content :: a    -- ^ The content of the leaf.
    }
    deriving Show
makeLenses ''LBSNode
makePrisms ''LBSNode

{-| A Left balanced Binary Search Tree.

   This is essentially a Binary Search Tree with special node definition
   containing the size of the subtree underneath and the actual content.
-}
type LBSTree a = BSTree (LBSNode a)

{-| A function used to compute the content of the parent of two nodes.
-}
type LBSPComputer m a = (a -> a -> ChronicleT Messages m a)

{-| A function used to construct the parent node of two other nodes.
-}
type BSPComputer m a = (LBSNode a -> LBSNode a -> ChronicleT Messages m (LBSNode a))

-----------------------------
--  Classes and instances  --
-----------------------------

instance Eq a => Eq (LBSNode a) where
  (LBSInternalNode s0 ls0 c0) == (LBSInternalNode s1 ls1 c1) = s0 == s1 && ls0 == ls1 && c0 == c1
  (LBSLeaf i0 c0) == (LBSLeaf i1 c1)                 = i0 == i1 && c0 == c1
  _ == _                                             = False

instance Eq a => Ord (LBSNode a) where
  compare n0 n1 = compare (key n0) (key n1)

instance Eq a => DynamicallyIndexable (LBSNode a) where
  dindex n@LBSInternalNode{} _ _ = n -- This should never happen though. Only a
                                     -- leaf should be used with BSTree search
                                     -- operations.
  dindex (LBSLeaf i c) ln _
    | i >= lbsSize ln = LBSLeaf (i - lbsSize ln) c
    | otherwise    = LBSLeaf i c

instance Functor LBSNode where
  fmap f (LBSInternalNode s ls c) = LBSInternalNode s ls (f c)
  fmap f (LBSLeaf i c)            = LBSLeaf i (f c)

----------------
--  Bindings  --
----------------

_LBSTREE_OUT_OF_RANGE_INDEX_ :: Text
_LBSTREE_OUT_OF_RANGE_INDEX_ = "LBSTree operation on out-of-range index."

_LBSTREE_UNIQUE_VALUE_ :: Text
_LBSTREE_UNIQUE_VALUE_ = "LBSTree should not contain duplicates."

_LBSTREE_ILLFORMED_NODE_ :: Text
_LBSTREE_ILLFORMED_NODE_ = "LBSTree had ill-formed nodes."

{-| The key of a `LBSNode` used to perform searches in the tree.
-}
key :: LBSNode a -> Int
key LBSInternalNode{_leftSubTreeSize = s} = s
key LBSLeaf{_index                   = i} = i

{-| Yields the size of a (sub)tree. This only counts leaves.
-}
lbsSize :: LBSNode a -> Int
lbsSize LBSLeaf{}                  = 1
lbsSize LBSInternalNode{_size = s} = s

{-| Yields the size of (sub)tree
-}
subtreeSize :: LBSTree a -> Int
subtreeSize = lbsSize . root

{-| Wraps a `LBSPComputer m a` inside a `BSPComputer a`. This effectively computes
   the sum of the size of two subtrees and let the `LBSPComputer m a` compute the
   content of the resulting internal node.
-}
bsPComputer :: Monad m => LBSPComputer m a -> BSPComputer m a
bsPComputer pcomp n0 n1 = do
  newContent <- view content n0 `pcomp` view content n1
  return $ LBSInternalNode (lbsSize n0 + lbsSize n1) (lbsSize n0) newContent


{-| Increment indexes of a subtree by `amount` units.
-}
incIndexes :: Monad m => Int -> LBSTree a -> ChronicleT Messages m (LBSTree a)
incIndexes amount = mapML incIndex
  where
    incIndex (LBSLeaf i c) = return $ LBSLeaf (i+amount) c
    incIndex _             = confess [ Msg Error callStack _LBSTREE_ILLFORMED_NODE_ ]

{-| Returns the node stored for the given index number.
-}
get :: Monad m =>
  Int          -- ^ The index of the node to update.
  -> LBSTree a -- ^ The tree in which the update is performed.
  -> ChronicleT Messages m (LBSNode a)
get indx = go indx
  where
    go _ (Leaf n@LBSLeaf{})
      | fromJust (preview index n) == indx = return n
      | otherwise                          = confess [Msg Error callStack _LBSTREE_OUT_OF_RANGE_INDEX_]
    go _ (Leaf _) = confess [Msg Error callStack _LBSTREE_ILLFORMED_NODE_]
    go i (Branch lt _ rt)
      | i < subtreeSize lt = go i lt
      | otherwise          = go (i - subtreeSize lt) rt

{-| Sets the value of a leaf in the tree.
-}
set :: Monad m =>
  LBSPComputer m a -- ^ Function to compute the content of a parent node.
  -> a             -- ^ The new value to store.
  -> Int           -- ^ The index of the node to update.
  -> LBSTree a     -- ^ The tree in which the update is performed.
  -> ChronicleT Messages m (LBSTree a)
set pcomp v indx bst = go indx bst
  where
    bspComp = bsPComputer pcomp
    go _ (Leaf n@LBSLeaf{})
      | indx == key n = return $ Leaf $ LBSLeaf indx v
      | otherwise     = confess [Msg Error callStack _LBSTREE_OUT_OF_RANGE_INDEX_]
    go _ (Leaf _) = confess [Msg Error callStack _LBSTREE_ILLFORMED_NODE_]
    go i (Branch lt _ rt)
      | i < subtreeSize lt = do
        nt  <- go i lt
        inode <- root nt `bspComp` root rt
        return $ Branch nt inode rt
      | otherwise = do
        nt <- go (i - subtreeSize lt) rt
        inode <- root lt `bspComp` root nt
        return $ Branch lt inode nt

{-| Maps a function over nodes of the copath of another node while accumulating
   a value.
-}
cpMapAccum :: Monad m =>
  LBSPComputer m a      -- ^ Function to compute the content of a parent node.
  -> (b -> a -> (b, a)) -- ^ Function to map over the nodes and accumulate.
  -> Int                -- ^ The index of the node to deduce the copath from.
  -> b                  -- ^ The accumulator.
  -> LBSTree a          -- ^ The tree in which the update is performed.
  -> ChronicleT Messages m (LBSTree a)
cpMapAccum pcomp f indx acc0 bst = snd <$> go indx acc0 bst
  where
    bspComp                 = bsPComputer pcomp
    mapOnCopathNode acc n   = over _2 (<$ n) $ f acc (view content n)
    mapOnTreeRootNode acc t = over _2 (setRoot t) $ mapOnCopathNode acc (root t)
    go _ acc l@(Leaf LBSLeaf{}) = return (acc, l)
    go _ _ (Leaf _)             = confess [Msg Error callStack _LBSTREE_ILLFORMED_NODE_]
    go i acc (Branch lt _ rt)
      | i < subtreeSize lt = do
        (acc', lt') <- go i acc lt
        let (acc'', rt') = mapOnTreeRootNode acc' rt
        inode <- root lt' `bspComp` root rt'
        return (acc'', Branch lt' inode rt')
      | otherwise = do
        (acc', rt') <- go (i - subtreeSize lt) acc rt
        let (acc'', lt') = mapOnTreeRootNode acc' lt
        inode <- root lt' `bspComp` root rt'
        return (acc'', Branch lt' inode rt')

----------------------------
--  Insertion / Deletion  --
----------------------------

{-| Inserts content at some index in the `LBSTree`.

   This operation preserves the left-balance property, i.e. that nodes are first
   inserted on the left side of the tree. A new section on the right is opened
   only when the tree is perfectly filled.
-}
insert :: (HasCallStack, Monad m, Eq a) =>
  LBSPComputer m a -- ^ Function to compute the content of a parent node.
  -> Int           -- ^ The index of the node to insert.
  -> a             -- ^ The content of the node.
  -> LBSTree a     -- ^ The tree to insert the content to.
  -> ChronicleT Messages m (LBSTree a)
insert pcomp indx a0 = fullInsertOrGo
  where
    fullInsertOrGo bst@(Branch lt _ rt)
      | subtreeSize lt == subtreeSize rt = do
        nparent <- newParent bst nleaf
        return $ Branch bst nparent nleaf
      | otherwise                        = go indx bst
    fullInsertOrGo bst = go indx bst
    n0               = LBSLeaf indx a0
    nleaf            = Leaf n0
    bspComp          = bsPComputer pcomp
    newParent b0 b1  = root b0 `bspComp` root b1
    go _ l@(Leaf n1@(LBSLeaf i1 a1))
      | a0 == a1  = l <$ dictate [Msg Warning callStack _LBSTREE_UNIQUE_VALUE_]
      | otherwise = do
        let i  = min indx i1
            n  = min n0 n1
            n' = max n0 n1
            ll = Leaf $ LBSLeaf i (view content n)
            rl = Leaf $ LBSLeaf (i+1) (view content n')
        nparent <- n `bspComp` n'
        return $ Branch ll nparent rl
    go _ (Leaf _) = confess [Msg Error callStack _LBSTREE_ILLFORMED_NODE_]
    go i (Branch lt _ rt)
      | i < subtreeSize lt = do
        nt  <- go i lt
        nrt <- incIndexes 1 rt
        nparent <- newParent nt nrt
        return $ Branch nt nparent nrt
      | otherwise                        = do
        nt <- go (i - subtreeSize lt) rt
        nparent <- newParent lt nt
        return $ Branch lt nparent nt

{-| Creates a `BSTree` from a list of values to be stored in leaves and a
   `BSPComputer` that computes the parent of two nodes.

   Nodes are going to be stored sorted in the same order yielded by the list,
   i.e. leaves at the base of the tree from left to right will have the same
   sequence ordering as in the list.
-}
fromList :: (HasCallStack, Monad m, Eq a) =>
  LBSPComputer m a -- ^ Function to compute the content of a parent node.
  -> [a]           -- ^ List of values to populate the tree.
  -> MaybeT (ChronicleT Messages m) (LBSTree a)
fromList _ []         = empty
fromList pcomp (a:as) = lift $ foldM accumulate iniLeaf $ zip [1..] as
  where
    iniLeaf                = Leaf $ LBSLeaf 0 a
    accumulate bst (i, a') = insert pcomp i a' bst

{-| Deletes the content at given index in the `LBSTree`.
-}
delete :: Monad m =>
  LBSPComputer m a -- ^ Function to compute the content of a parent node.
  -> Int           -- ^ The index of the element to remove.
  -> LBSTree a     -- ^ The tree to remove the element from.
  -> MaybeT (ChronicleT Messages m) (LBSTree a)
delete pcomp indx = go indx
  where
    bspComp                   = bsPComputer pcomp
    merge Nothing Nothing     = empty
    merge Nothing (Just rt)   = return rt
    merge (Just lt) Nothing   = return lt
    merge (Just lt) (Just rt) = do
      nparent <- lift $ root lt `bspComp` root rt
      return $ Branch lt nparent rt
    go _ (Leaf n)
      | indx == key n = empty
      | otherwise     = lift $ confess [Msg Error callStack _LBSTREE_OUT_OF_RANGE_INDEX_]
    go i (Branch lt _ rt)
      | i < subtreeSize lt = do
        nt <- lift $ runMaybeT $ go i lt
        nrt <- lift $ Just <$> incIndexes (-1) rt
        merge nt nrt
      | otherwise = lift (runMaybeT (go (i - subtreeSize lt) rt)) >>= merge (Just lt)

{-| Deletes the last element from the list of leaves.

   The complexity of this is \(\mathcal{O}\left(log(n)\right)\) since there is
   no subtree on the right to be edited. This is the best case.
-}
init :: Monad m =>
  LBSPComputer m a -- ^ Function to compute the content of a parent node.
  -> LBSTree a     -- ^ The `LBSTree` to modify.
  -> MaybeT (ChronicleT Messages m) (LBSTree a)
init pcomp bst = delete pcomp (subtreeSize bst - 1) bst

{-| Deletes the first element from the list of leaves.

   The complexity of this is \(\theta(n)\) since all elements to the right
   have to be edited so that internal indexes are consistent with the new
   left-balanced tree structure resulting from the deletion of the element. This
   is the worst case.
-}
tail :: Monad m =>
  LBSPComputer m a -- ^ Function to compute the content of a parent node.
  -> LBSTree a     -- ^ The `LBSTree` to modify.
  -> MaybeT (ChronicleT Messages m) (LBSTree a)
tail = flip delete 0

--  vim: set sts=2 ts=2 sw=2 tw=120 et :

