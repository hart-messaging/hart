
{-|
  Module      : Hart.Internal.Data.BSTree
  Description : Binary Search Tree data types and bindings.
  Copyright   : (c) Simon Désaulniers, 2020
  License     : GPL-3

  Maintainer  : sim.desaulniers@gmail.com

  This modules exposes functions and types for working with a [binary search
  tree](https://fr.wikipedia.org/wiki/Binary_search_tree). This binary search
  tree has the specificity of yielding elements only in the leaves.
  In other words, internal nodes cannot be the target of a search
  operations.

  The bindings exposed are designed to support the "Hart.Internal.Tree" module
  with appropriate functions such as the non standard `copath` function.

  Various utility functions such as `root`, `internals` and `leaves` are
  provided.
-}

{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts #-}

module Hart.Internal.Data.BSTree ( BSTree (..)
                                 , DynamicallyIndexable (..)
                                 , BSTreeAlgebra
                                 , mapL
                                 , mapML
                                 , catamorph
                                 , root
                                 , setRoot
                                 , internals
                                 , leaves
                                 , pFold
                                 , path
                                 , copath
                                 ) where

import Data.Maybe

import Data.Text (Text)

import Control.Monad

import GHC.Stack

import Colog.Core
import Colog.Message

import Control.Monad.Chronicle

---------------------
--  Local modules  --
---------------------

import Hart.Internal.Log

-------------
--  Types  --
-------------

{-| A binary search tree.
-}
data BSTree a = Leaf a
              | Branch (BSTree a) a (BSTree a)

{-| A binary search tree [initial
   algebra](https://en.wikipedia.org/wiki/Initial_algebra).

   It is used in order to perform a catamorphism on a BSTree which provides the
   exact process we need for traversing a binary tree along a given path. See
   `catamorph` and `path`.
-}
type BSTreeAlgebra a r = (a -> r -> r, a -> r -> r -> r)

-----------------------------
--  Classes and instances  --
-----------------------------

instance Functor BSTree where
  fmap f (Leaf a)         = Leaf $ f a
  fmap f (Branch lt a rt) = Branch (fmap f lt) (f a) (fmap f rt)

instance Foldable BSTree where
   foldr f acc (Leaf x)       = f x acc
   foldr f acc (Branch l k r) = foldr f (f k (foldr f acc r)) l

instance Eq a => Eq (BSTree a) where
  Leaf v0           == Leaf v1           = v0  == v1
  Branch lt0 r0 rt0 == Branch lt1 r1 rt1 = lt0 == lt1 && rt0 == rt1 && r0 == r1
  _                 == _                 = False

{-| Yields the new node and tree combination to work on. Searches on our type of
   Binary Search tree work on dynamic keys, i.e. that keys may change depending
   on the path our node is (left or right).

   Indeed, the canonical behaviour of a Binary Search tree with static keys can
   be retrieved by implementing `dindex` with @\ a _ _ -> a@.
-}
class Ord a => DynamicallyIndexable a where
  dindex ::
    a    -- ^ The node that is currently searched.
    -> a -- ^ The left child.
    -> a -- ^ The right child.
    -> a -- ^ The new key to look for inside the new (sub)tree.

----------------
--  Bindings  --
----------------

_BSTREE_OPERATION_ON_EMPTY_TREE_ :: Text
_BSTREE_OPERATION_ON_EMPTY_TREE_ = "BSTree is empty. This should not happen..."

_BSTREE_OPERATION_ON_UNKNOWN_NODE_ :: Text
_BSTREE_OPERATION_ON_UNKNOWN_NODE_ =
  "BSTree doesn't contain the researched node.\n\
  \Explanation: The node should at least populate the Tree so this\n\
  \should not happen..."

{-| [Catamorphism](https://en.wikipedia.org/wiki/Catamorphism) on a `BSTree`.

-}
catamorph :: BSTreeAlgebra a r -> r -> BSTree a -> r
catamorph (f, _) acc (Leaf v)         = f v acc
catamorph (f, g) acc (Branch lt v rt) = g v (catamorph (f, g) acc lt) (catamorph (f, g) acc rt)

{-| Map a function onto leaves of the tree. Indeed, the type of the resulting
   tree needs to stay unchanged compared to the input tree.
-}
mapL :: (a -> a) -> BSTree a -> BSTree a
mapL f = fromJust . catamorph (\ a _ -> return $ Leaf $ f a, joinSubtrees) Nothing
  where
    joinSubtrees v jlt jrt = return $ Branch (fromJust jlt) v (fromJust jrt)

{-| Maps function on leaves inside of a Monad (like `mapM`).
-}
mapML :: Monad m => (a -> m a) -> BSTree a -> m (BSTree a)
mapML f (Leaf a)         = Leaf <$> f a
mapML f (Branch lt a rt) = do
  nlt <- mapML f lt
  nrt <- mapML f rt
  return $ Branch nlt a nrt

{-| An accessor for the root of a `BSTree`.
-}
root :: BSTree a -> a
root (Leaf v)       = v
root (Branch _ v _) = v

setRoot :: BSTree a -> a -> BSTree a
setRoot (Branch lt _ rt) n = Branch lt n rt
setRoot l@Leaf{} n         = n <$ l

{-| Returns the list of internal nodes in the `BSTree`.
-}
internals :: BSTree a -> [a]
internals = catamorph (const id, \ v l r -> l ++ [v] ++ r) []

{-| Returns the list of leaves in the `BSTree`.
-}
leaves :: BSTree a -> [a]
leaves = catamorph ((:), const (++)) []

{-| Fold over a path in the a binary search tree.
-}
pFold :: (DynamicallyIndexable a) => a -> (a -> b -> b) -> b -> BSTree a -> b
pFold _ f acc (Leaf v)         = f v acc
pFold k f acc (Branch lt r rt) = f r $ pFold (dindex k lr rr) f acc $ if k < r then lt else rt
  where
    lr = root lt
    rr = root rt

{-| Returns the list of nodes consisting in the path from the root of the
   `BSTree` to the given node.
-}
path :: (HasCallStack, Monad m, DynamicallyIndexable a) =>
  a           -- ^ The node.
  -> BSTree a -- ^ The binary search tree.
  -> ChronicleT Messages m [a]
path v0 bst = do
  let p = pFold v0 (:) [] bst
  when (v0 /= last p) $ confess [Msg Error callStack _BSTREE_OPERATION_ON_UNKNOWN_NODE_]
  return p

{-| Returns the copath of a node. Let \(T\) denote the `BSTree` and \(p\) the
   path to some node \(n\). The copath of \(n\) is the list of nodes
   \(\{x \in T\ |\ x\ \text{is a sibling of a node in p}\ \}\).
-}
copath :: (HasCallStack, Monad m, DynamicallyIndexable a) =>
  a           -- ^ The node.
  -> BSTree a -- ^ The binary search tree.
  -> ChronicleT Messages m [a]
copath v0 = go v0
  where go v (Branch lt v' rt)
          | v < v'    = (:) rr <$> go nv lt
          | otherwise = (:) lr <$> go nv rt
          where
            lr = root lt
            rr = root rt
            nv = dindex v lr rr
        go _ (Leaf v') = do
          when (v0 /= v') $ confess [Msg Error callStack _BSTREE_OPERATION_ON_UNKNOWN_NODE_]
          return []

--  vim: set sts=2 ts=2 sw=2 tw=120 et :

