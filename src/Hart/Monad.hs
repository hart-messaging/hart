
{-|
  Module      : Hart.Monad
  Description : The Hart monad transformer module.
  Copyright   : (c) Simon Désaulniers, 2021
  License     : GPL-3

  Maintainer  : sim.desaulniers@gmail.com

  This module contains the bindings and definitions for unwrapping a
  computation wrapped in the monads stack of functions found in "Hart.Tree".
-}

{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module Hart.Monad ( HartLogAction (..)
                  , HartTreeT (..)
                  , runHartTreeT
                  ) where

import Control.Monad.State
import Control.Monad.Reader

import Colog.Message
import Colog.Core

---------------------
--  Local modules  --
---------------------

import Hart.DiffieHellman

-------------
--  Types  --
-------------

{-| A monad transformer for running all of the Hart operations. This type wraps
   around StateT and ReaderT in order to satisfy the MonadState and HasLog
   constraints.

  __NOTE__: This is newtype instead of type since we want to force going through
  `runHartTreeT` to run this monad transformer. Using @type@ would enable using
  @(runReaderT . runStateT)@ which would be fine, but this would imply more
  boilerplate on the library user's side.
-}
newtype HartTreeT z m a = HartTreeT { unwrapHartTreeT :: StateT (DHMaterial z) (ReaderT (HartLogAction m z Message) m) a
                                    }
  deriving (Functor, Applicative, Monad, MonadReader (HartLogAction m z Message), MonadState (DHMaterial z))

instance MonadTrans (HartTreeT z) where
  lift = HartTreeT . lift . lift

{-| A wrapper for `LogAction` with a restriction on the the appropriate types we
   need as underlying monads in which we run our code, i.e. a stack of the
   monads `StateT` and `ReaderT` which satisfy the monad constraints
   `MonadState` and `HasLog` on our main API functions.
-}
newtype HartLogAction m z msg = HartLogAction { envLogAction :: LogAction (HartTreeT z m) msg }

instance HasLog (HartLogAction m z msg) msg (HartTreeT z m) where
  getLogAction            = envLogAction
  setLogAction newEnv env = env { envLogAction = newEnv }

----------------
--  Bindings  --
----------------

{-| This functions unwraps the computation taking place in HartTreeT just like
   any usual @run*@ functions. It is in fact a wrapper around `runReaderT` and
   `evalStateT`.
-}
runHartTreeT :: Monad m =>
  HartTreeT z m a
  -> HartLogAction m z Message
  -> DHMaterial z
  -> m a
runHartTreeT hartTreeTcomputation logAction z = runReaderT (evalStateT (unwrapHartTreeT hartTreeTcomputation) z) logAction

--  vim: set sts=2 ts=2 sw=2 tw=120 et :

