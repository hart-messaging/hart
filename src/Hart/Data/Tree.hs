
{-|
  Module      : Hart.Data.Tree
  Description : Datatypes, classes and instances for the ART tree.
  Copyright   : (c) Simon Désaulniers, 2020
  License     : GPL-3

  Maintainer  : sim.desaulniers@gmail.com

  This modules exposses data types for working with the binary tree structure as
  described by the [ART
  protocol](https://people.cispa.io/cas.cremers/downloads/papers/CCGMM2018-groupmessaging.pdf).
  For associated functions, see module "Hart.Tree".

  The binary tree is the structure that encodes information about public keys
  and identifiers of the users in the group discussion. Users information are
  based in the leaves of the tree and computation done by users together are
  stored in the internal nodes.

  The binary tree type used is `LBSTree`. See the module "Hart.Internal.Data.LBSTree"
  for more information about this.
-}

{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE TemplateHaskell #-}

module Hart.Data.Tree where

import Data.Set (Set)
import Data.ByteString (ByteString)

import Control.Lens

---------------------
--  Local modules  --
---------------------

import Hart.Internal.Data.LBSTree

-------------
--  Types  --
-------------

type UserID = ByteString

{-| Information stored inside an internal node of the `ArtTree` which contains
   computation of its two children's keys.

   Only the internal nodes along the `path` from the root to the local user's
   info node has private keys.
-}
data InternalNodeInfo v b = InternalNodeInfo
  { _internalNodePrivateKey :: Maybe v -- ^ The private key of this subtree.
  , _internalNodePublicKey  :: Maybe b -- ^ The public key of this subtree.
  }
  deriving Eq
makeLenses ''InternalNodeInfo

{-| Information stored inside a leaf node of the `ArtTree`. This is either
   the public key information of a remote user (`RemoteUser`), public and
   private keys of the local user (`LocalUser`) or finally the public and
   private keys of a remote user when initializing a tree locally before
   exchanging any message with the remote user (`StartUpRemoteUser`).
-}
data UserInfo v b =
  StartUpRemoteUser
    { _userPrivateKey :: v  -- ^ The startup key when the tree is created by the
                            --   administrator.
    , _userPublicKey :: b   -- ^ The public key of the remote user.
    }
  | RemoteUser
    {
      _userPublicKey :: b -- ^ The public key of the remote user.
    }
  | LocalUser
    { _userPrivateKey :: v -- ^ The private key of the local user
    , _userPublicKey  :: b -- ^ The public key of the local user
    }
    deriving (Eq, Show)
makePrisms ''UserInfo
makeLenses ''UserInfo

{-| A node of the `ArtTree`.
-}
type ArtNode v b = Either (InternalNodeInfo v b) (UserInfo v b)

{-| The tree at the base of the protocol which contains the users' info and
   public keys.
-}
data ArtTree v b = ArtTree
  { treeData :: LBSTree (ArtNode v b) -- ^ The `LBSTree` containing the tree data.
  , treeUIDs :: Set UserID            -- ^ Ordered set of UIDs.
  }

--  vim: set sts=2 ts=2 sw=2 tw=120 et :

