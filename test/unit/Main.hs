
module Main where

import System.IO

import Control.Monad

import Test.HUnit

import qualified HartTest.Internal.Data.LBSTree as LBST
import qualified HartTest.Internal.Data.BSTree as BST
import qualified HartTest.Internal.Tree as IT
import qualified HartTest.Tree as T

tests :: [Test]
tests = [ "Hart.Internal.Data.BSTree"  ~: TestList BST.tests
        , "Hart.Internal.Data.LBSTree" ~: TestList LBST.tests
        , "Hart.Internal.Tree"         ~: TestList IT.tests
        , "Hart.Tree"                  ~: TestList T.tests
        ]

runLabeledModuleTestSuite :: Test -> IO ()
runLabeledModuleTestSuite (TestLabel module' t) = do
  hPutStrLn stderr $ "Executing tests for " ++ module'
  _ <- runTestTT t
  hPutStrLn stderr ""
runLabeledModuleTestSuite _ = error "Module test suites should be labeled..."

main :: IO ()
main = forM_ tests runLabeledModuleTestSuite

--  vim: set sts=2 ts=2 sw=2 tw=120 et :

