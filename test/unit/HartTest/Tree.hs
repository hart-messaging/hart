
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE ScopedTypeVariables #-}

{-| Unit testing for the "Hart.Tree" module.
-}
module HartTest.Tree (
  tests
) where

import Data.Text ()
import Data.Array
import qualified Data.List as L

import Data.Maybe
import Data.Bifunctor
import Data.Char (ord)
import qualified Data.Set as Set
import qualified Data.ByteString as BS
import Control.Monad.Trans.Maybe

import Control.Lens
import Control.Monad
import Control.Monad.State (MonadState)
import Control.Monad.Reader

import Colog.Core
import Colog.Message
import Colog.Monad

import Test.HUnit

---------------------
--  Local modules  --
---------------------

import Hart.Log
import Hart.Tree
import Hart.Monad
import Hart.DiffieHellman
import Hart.Data.Tree
import Hart.Internal.Data.LBSTree

import HartTest.Common

_HART_TREE_ :: String
_HART_TREE_ = "Hart.Tree"

tests :: [Test]
tests = [ test_users
        , test_rootKey
        , test_copath
        , test_initializeLocally
        , test_initializeFromRemoteData
        , test_addUser
        , test_removeUser
        , test_updateKey
        ]

-----------------------
--  Local utilities  --
-----------------------

newtype MyDHComputableInteger = MyDHComputableInteger { getMyDHComputableInteger :: Integer }
  deriving (Show, Eq)

instance DHComputable Integer MyDHComputableInteger MyDHComputableInteger where
  (MyDHComputableInteger vk) `dhExchange` (MyDHComputableInteger pk) = return $ MyDHComputableInteger $ pk^vk
  exponentiate (MyDHComputableInteger k) = do
    g <- use generator
    return $ MyDHComputableInteger $ g ^ k

bstringFromInteger :: Integer -> BS.ByteString
bstringFromInteger = BS.pack . map (fromIntegral . ord) . show

aggregateRightLeftTree ::
  Maybe (LBSTree (ArtNode v b))
  -> (Int, (Int, UserInfo v b))
  -> Maybe (LBSTree (ArtNode v b))
aggregateRightLeftTree Nothing (_, (i, ui)) = return $ leaf i (Right ui)
aggregateRightLeftTree (Just t) (idx, (i, ui))
    | even idx    = do
      let lsize = fromMaybe 1 (root t^?leftSubTreeSize)
      return $ Branch t (LBSInternalNode (lsize+1) lsize inode_content) (leaf i (Right ui))
    | otherwise = do
      let rsize = fromMaybe 1 (root t^?size)
      return $ Branch (leaf i (Right ui)) (LBSInternalNode (rsize+1) 1 inode_content) t
  where inode_content = Left (InternalNodeInfo Nothing Nothing)

{-| Make a right-left LBSTree of ArtNode.

  @
            o
           / \
          o   3
         / \
        0   o right
           / \
     left 1   2
  @

  A right-left tree alternates left and right. The most important characteristic
  that we want is for the tree to have a right turn, then a left turn (hence
  right-left tree) in order for the tests to check for a particular behaviour of
  the LBSTree which is to properly handle the nodes index while searching inside.

  NOTE: This approach doesn't fit very well for testing functions which return
  values depending on the content of internal nodes since we're simply avoiding
  to create internal nodes with content other than "null". Therefore, it won't
  be possible to use this for testing `Hart.Tree.initializeLocally` and
  `Hart.Tree.initializeFromRemoteData` for instance. Providing a way to build
  internal nodes would mean to recreate the code logic we're trying to test (the
  one of LBSPComputer more precisely). Thus, it's a better approach to do like
  in HartTest.Internal.Data.LBSTree and recreate the tree structure manually
  using the associated data constructors.
-}
makeRightLeftTree :: [Int] -> [UserInfo v b] -> LBSTree (ArtNode v b)
makeRightLeftTree rlIndices uinfos = fromJust . L.foldl' aggregateRightLeftTree Nothing $ zip [1..] $ zip rlIndices uinfos

-- The sequence of a right left tree is as follows:
-- @
--   n | sequence
--   - | --------
--   1 | 0
--   2 | 0 1
--   3 | 1 2 0
--   4 | 1 2 0 3
--   5 | 2 3 1 4 0
-- @
rightLeftIndices :: Int -> [Int]
rightLeftIndices n = foldl genNext [] [1..n]
  where
    genNext mySeq i
      | even i    = mySeq ++ [i-1]
      | otherwise = map (+1) mySeq ++ [0]


extractKeys :: UserInfo v b -> (Maybe v, Maybe b)
extractKeys ui = (ui^?userPrivateKey, ui^?userPublicKey)

makeIdUserInfoPair ::
  Integer
  -> (MyDHComputableInteger
  -> MyDHComputableInteger
  -> UserInfo MyDHComputableInteger MyDHComputableInteger)
  -> (UserID, UserInfo MyDHComputableInteger MyDHComputableInteger)
makeIdUserInfoPair n toUserInfo = (user_id, user_info)
  where
    user_vk   = MyDHComputableInteger n
    user_pk   = MyDHComputableInteger n
    user_info = toUserInfo user_vk user_pk
    user_id   = bstringFromInteger (getMyDHComputableInteger user_vk)

-------------
--  Tests  --
-------------

test_users :: Test
test_users = _HART_TREE_ ++ ".users" ~: [ singleton
                                        , twoNodes
                                        ]
  where
    fname = "users"
    singleton = "testing for singleton tree" ~: TestCase $ do
      let
        vk        = 0 :: Int
        pk        = 0 :: Int
        user_id   = BS.pack $ map (fromIntegral . ord) "mySuperUserId"
        user_info = LocalUser vk pk
        arttree   = ArtTree (leaf 0 (Right user_info)) (Set.fromList [user_id])
      users_info <- usingLoggerT toStdErr $ users arttree
      assertEqual (fname ++ " " ++ "tree (singleton)") [extractKeys user_info] (map extractKeys users_info)

    twoNodes = "testing for two nodes tree" ~: TestCase $ do
      let
        local_user_vk     = 0 :: Integer
        local_user_pk     = 0 :: Integer
        local_user_id     = bstringFromInteger local_user_pk
        local_user_info   = LocalUser local_user_vk local_user_pk
        remote_user_pk    = 1 :: Integer
        remote_users_info = RemoteUser remote_user_pk
        remote_user_id    = bstringFromInteger remote_user_pk

        my_rl_indices = rightLeftIndices 2
        tree          = makeRightLeftTree my_rl_indices [local_user_info, remote_users_info]
        user_ids_set  = Set.fromList [local_user_id, remote_user_id]
        arttree       = ArtTree tree user_ids_set

        expectedRetUsers = [extractKeys local_user_info, extractKeys remote_users_info]

      retUsers <- usingLoggerT toStdErr $ users arttree
      assertEqual (fname ++ " " ++ "tree (2 nodes)") expectedRetUsers $ map extractKeys retUsers

test_rootKey :: Test
test_rootKey = _HART_TREE_ ++ ".rootKey" ~: [ singleton
                                            , twoNodes
                                            ]
  where
    fname = "rootKey"
    singleton = "testing for singleton tree" ~: TestCase $ do
      let
        (user_id, user_info)   = makeIdUserInfoPair 0 LocalUser
        arttree     = ArtTree (leaf 0 (Right user_info)) (Set.fromList [user_id])
        expected_vk = MyDHComputableInteger 0
      mvk <- usingLoggerT toStdErr $ runMaybeT $ rootKey arttree
      assertBool (fname ++ " " ++ "singleton (treeData)") $ Just expected_vk == mvk
    twoNodes  = "testing for two-nodes tree" ~: TestCase $ do
      let
        (first_user_id, first_user_info)   = makeIdUserInfoPair 0 LocalUser
        (second_user_id, second_user_info) = makeIdUserInfoPair 1 (\ _ pk -> RemoteUser pk)

        user_ids_set = Set.fromList [first_user_id, second_user_id]
        t = do
          root_vk <- dhExchange (first_user_info^?!userPrivateKey) (second_user_info^?!userPublicKey)
          root_pk <- exponentiate root_vk
          let
            arttree = ArtTree tree user_ids_set
            tree    = Branch
                        (leaf 0 (Right first_user_info))
                        (LBSInternalNode 2 1 $ Left $ InternalNodeInfo (Just root_vk) (Just root_pk))
                        (leaf 1 (Right second_user_info))
          mvk <- runMaybeT $ rootKey arttree
          lift $ assertBool (fname ++ " " ++ "two nodes (treeData)") $ Just root_vk == mvk
      runHartTreeT t hartToStdErr (DHMaterial (0 :: Integer))

-------------------
--  test_copath  --
-------------------

type CopathExpectedOut = Maybe [ArtNode Integer Integer]
data CopathTestCase    = CopathValidTestCase { targetNodeIndex :: Integer
                                             , expectedOutput  :: CopathExpectedOut
                                             }
                       | CopathInvalidTestCase { targetNodeIndex :: Integer
                                               , expectedOutput  :: CopathExpectedOut
                                               }

test_copath :: Test
test_copath = _HART_TREE_ ++ ".copath" ~: [ singleton
                                          , twoNodes
                                          , rightLeft
                                          ]
  where
    fname = "copath"
    -- shared bindings among tests
    local_user_vk   = 0 :: Integer
    local_user_pk   = 0 :: Integer
    local_user_info = LocalUser local_user_vk local_user_pk
    local_user_id   = bstringFromInteger local_user_pk

    -- tests
    singleton = "testing for singleton" ~: TestCase $ do
      let arttree        = ArtTree (leaf 0 (Right local_user_info)) (Set.fromList [local_user_id])
          expectedCopath = Just [ ] -- single node has an empty copath.
      mcopath <- usingLoggerT toStdErr $ runMaybeT $ Hart.Tree.copath local_user_id arttree
      assertBool (fname ++ " " ++ "(singleton tree) for UserID " ++ show local_user_id) $ mcopath == expectedCopath
    twoNodes = "testing for two nodes tree." ~: TestCase $ do
      let
        remote_user_pk   = 1 :: Integer
        remote_user_id   = bstringFromInteger remote_user_pk
        remote_user_info = RemoteUser remote_user_pk
        inode_content    = Left (InternalNodeInfo Nothing Nothing)
        tree             = Branch
                            (leaf 0 (Right local_user_info))
                            (LBSInternalNode 2 1 inode_content)
                            (leaf 1 (Right remote_user_info))
        arttree          = ArtTree tree (Set.fromList [local_user_id, remote_user_id])
        expectedCopath   = Just [ Right remote_user_info ]
      mcopath <- usingLoggerT toStdErr $ runMaybeT $ Hart.Tree.copath local_user_id arttree
      assertBool (fname ++ " " ++ "(singleton tree) for UserID " ++ show local_user_id) $ mcopath == expectedCopath
    rightLeft = "testing for right-left tree" ~: TestCase $ do
      let
        -- Keys are taken from integer numbers in order to easily deduce order
        -- in the bytestring format.
        remote_pks      = [1, 2]
        remote_user_ids = map bstringFromInteger remote_pks
        users_info      = map RemoteUser remote_pks

        my_rl_indices       = rightLeftIndices 3
        my_unordered_uinfos = local_user_info : users_info

        myOrderedUinfos :: Array Integer (UserInfo Integer Integer)
        myOrderedUinfos = array (0 ,2) [(i, my_unordered_uinfos !! j) | (i, j) <- zip [0..] my_rl_indices]

        tree         = makeRightLeftTree my_rl_indices $ elems myOrderedUinfos
        user_ids_set = Set.fromList $ bstringFromInteger local_user_pk : remote_user_ids
        arttree      = ArtTree tree user_ids_set

      let
        testCopath :: Integer -> UserID -> CopathExpectedOut -> LogAction IO Message -> Assertion
        testCopath tindex user_id expectedCopath loggingMethod = do
          mcopath <- usingLoggerT loggingMethod $ runMaybeT $ Hart.Tree.copath user_id arttree
          assertBool (fname ++ " " ++ "(right-left tree) for UserID " ++ show user_id ++ " with tree index " ++ show tindex)
                     $ mcopath == expectedCopath
        userIdFromIndex tindex
            | tindex `elem` [0..n-1] = Set.elemAt tindex user_ids_set            -- Valid userIds from the set.
            | otherwise              = bstringFromInteger (fromIntegral tindex)  -- Out of range element for test purposes
                                                                                 -- (invalid cases).
          where n = Set.size user_ids_set

        iterateCopathTestCase (CopathValidTestCase   i expectedOut) = _testCopath i expectedOut toStdErr -- logging enabled
        iterateCopathTestCase (CopathInvalidTestCase i expectedOut) = _testCopath i expectedOut mempty   -- logging disabled
        _testCopath i expectedOut                                   = testCopath i (userIdFromIndex (fromIntegral i)) expectedOut

      -- Below is depicted the respective situations following the tree structure:
      --
      --    o
      --   / \
      --  0   o
      --     / \
      --    1   2
      --
      -- Given that n is a node, (n) and [n] respectively mean that n is on the
      -- path and the copath of the target node.
      --
      -- Taget node: 0 | Target node: 1 | Target node: 2
      --               |                |
      --   (o)         |    (o)         |    (o)
      --   / \         |    / \         |    / \
      -- (0) [o]       |  [0] (o)       |  [0] (o)
      --     / \       |      / \       |      / \
      --    1   2      |    (1) [2]     |    [1] (2)
      --

      let
        validcases = [ CopathValidTestCase { targetNodeIndex = 0
                                           , expectedOutput  = Just [ Left (InternalNodeInfo Nothing Nothing)    ]
                                           }
                     , CopathValidTestCase { targetNodeIndex = 1
                                           , expectedOutput  = Just [ Right local_user_info , Right (RemoteUser 2) ]
                                           }
                     , CopathValidTestCase { targetNodeIndex = 2
                                           , expectedOutput  = Just [ Right local_user_info , Right (RemoteUser 1) ]
                                           }
                     ]
        invalidcases = [ CopathInvalidTestCase { targetNodeIndex = -1
                                               , expectedOutput  = Nothing
                                               }
                       , CopathInvalidTestCase { targetNodeIndex = 3
                                               , expectedOutput  = Nothing
                                               }
                       ]

      mapM_ iterateCopathTestCase validcases
      mapM_ iterateCopathTestCase invalidcases

expectedTreeFromInitialization :: (MonadState (DHMaterial z) m, DHComputable z v b)
                               => UserInfo v b
                               -> UserInfo v b
                               -> UserInfo v b
                               -> m (LBSTree (ArtNode v b))
expectedTreeFromInitialization local_user_info r1 r2 = do
  -- The ART key exchange
  zero_one_sub_tree_priv_key <- dhExchange (local_user_info^?!userPrivateKey) (r1^?!userPublicKey)
  zero_one_sub_tree_pub_key  <- exponentiate zero_one_sub_tree_priv_key
  root_priv_key              <- dhExchange zero_one_sub_tree_priv_key (r2^?!userPublicKey)
  root_pub_key               <- exponentiate root_priv_key
  let
    leftSubTreeInternalArtNode = Left $ InternalNodeInfo (Just zero_one_sub_tree_priv_key) (Just zero_one_sub_tree_pub_key)
    rootArtNode                = Left $ InternalNodeInfo (Just root_priv_key) (Just root_pub_key)
    expected_tree = Branch
                    (Branch
                      (leaf 0 (Right local_user_info))
                      (LBSInternalNode 2 1 leftSubTreeInternalArtNode)
                      (leaf 1 (Right r1)))
                    (LBSInternalNode 3 2 rootArtNode)
                            (leaf 2 (Right r2))
  return expected_tree

test_initializeLocally :: Test
test_initializeLocally = _HART_TREE_ ++ ".initializeLocally" ~: [ emptyList
                                                                , nonEmptyList
                                                                ]
  where
    fname = "initializeLocally"
    emptyList = "testing for empty list" ~: TestCase $ do
      let
        the_empty_list = [] :: [(UserID, UserInfo MyDHComputableInteger MyDHComputableInteger)]
        t              = runMaybeT $ Hart.Tree.initializeLocally the_empty_list
      mt <- runHartTreeT t hartNoLogging (DHMaterial (0 :: Integer))
      assertBool (fname ++ " " ++ "emptyList (treeData)") $ isNothing mt
    nonEmptyList = "testing for non empty list" ~: TestCase $ do
      let
        local_user_vk   = MyDHComputableInteger 0
        local_user_pk   = MyDHComputableInteger 0
        local_user_info = LocalUser local_user_vk local_user_pk
        local_user_id   = bstringFromInteger (getMyDHComputableInteger local_user_pk)

        remote_key_pairs  = map (join bimap MyDHComputableInteger) [(1,1), (2,2)]
        remote_pks        = map snd remote_key_pairs
        remote_user_ids   = map (bstringFromInteger . getMyDHComputableInteger) remote_pks
        remote_users_info = map (uncurry StartUpRemoteUser) remote_key_pairs
        initializeAndTestResult = do
          let
            user_id_info_pairs    = (local_user_id, local_user_info) : zip remote_user_ids remote_users_info
            expected_user_ids_set = Set.fromList $ local_user_id : remote_user_ids
          expected_tree <- expectedTreeFromInitialization local_user_info (head remote_users_info) (last remote_users_info)
          mt            <- runMaybeT $ Hart.Tree.initializeLocally user_id_info_pairs

          lift $ assertBool (fname ++ " " ++ "nonEmptyList (treeData)") $ maybe False ((== expected_tree) . treeData) mt
          lift $ assertBool (fname ++ " " ++ "nonEmptyList (treeUIDs)") $ maybe False ((== expected_user_ids_set) . treeUIDs) mt
      runHartTreeT initializeAndTestResult hartToStdErr (DHMaterial { _generator = 2 :: Integer })

test_initializeFromRemoteData :: Test
test_initializeFromRemoteData = _HART_TREE_ ++ ".initializeFromRemoteData" ~: [ emptyList
                                                                              , nonEmptyList
                                                                              ]
  where
    fname = "initializeFromRemoteData"
    emptyList = "testing for empty list" ~: TestCase $ do
      let the_empty_list = [] :: [(UserID, UserInfo MyDHComputableInteger MyDHComputableInteger)]
      mt <- runHartTreeT (runMaybeT $ initializeFromRemoteData the_empty_list []) hartNoLogging (DHMaterial (0 :: Integer))
      assertBool (fname ++ " " ++ "emptyList (treeData)") $ isNothing mt
    nonEmptyList = "testing for non empty list" ~: TestCase $ do
      let
        local_user_vk   = MyDHComputableInteger 0
        local_user_pk   = MyDHComputableInteger 0
        local_user_info = LocalUser local_user_vk local_user_pk
        local_user_id   = bstringFromInteger (getMyDHComputableInteger local_user_pk)

        remote_pks        = map MyDHComputableInteger [1, 2]
        remote_user_ids   = map (bstringFromInteger . getMyDHComputableInteger) remote_pks
        remote_users_info = map RemoteUser remote_pks
        initializeAndTestResult = do
          let
            user_id_info_pairs    = (local_user_id, local_user_info) : zip remote_user_ids remote_users_info
            expected_user_ids_set = Set.fromList $ local_user_id : remote_user_ids
          expected_tree <- expectedTreeFromInitialization local_user_info (head remote_users_info) (last remote_users_info)
          mt            <- runMaybeT $ initializeFromRemoteData user_id_info_pairs remote_pks
          lift $ assertBool (fname ++ " " ++ "nonEmptyList (treeData)") $ maybe False ((== expected_tree) . treeData) mt
          lift $ assertBool (fname ++ " " ++ "nonEmptyList (treeUIDs)") $ maybe False ((== expected_user_ids_set) . treeUIDs) mt
      runHartTreeT initializeAndTestResult hartToStdErr (DHMaterial { _generator = 2 :: Integer })

test_addUser :: Test
test_addUser = _HART_TREE_ ++ ".addUser" ~: [ noLocalUser
                                            , withLocalUser
                                            ]
  where
    fname = "addUser"
    noLocalUser = "testing for non empty tree without local user" ~: TestCase $ do
      let
        (new_user_id, new_user_info)        = makeIdUserInfoPair 0 (\ _ b -> RemoteUser b)
        (initial_user_id, initialuser_info) = makeIdUserInfoPair 1 (\ _ b -> RemoteUser b)

        non_sensical_pk_value = MyDHComputableInteger 0
        initial_bs_tree       = leaf 0 $ Right initialuser_info
        initial_user_ids_set  = Set.fromList [initial_user_id]
        initial_art_tree      = ArtTree initial_bs_tree initial_user_ids_set
        t = do
          mt <- runMaybeT $ Hart.Tree.addUser new_user_id new_user_info non_sensical_pk_value initial_art_tree
          lift $ assertBool (fname ++ " " ++ "noLocalUser") $ isNothing mt
      runHartTreeT t hartNoLogging (DHMaterial (0 :: Integer))
    withLocalUser = "testing for non empty tree with local user" ~: TestCase $ do
      let
        (initial_user_id, initialuser_info) = makeIdUserInfoPair 0 LocalUser
        (new_user_id, new_user_info)        = makeIdUserInfoPair 1 (\ _ b -> RemoteUser b)

        initial_bs_tree      = leaf 0 $ Right initialuser_info
        initial_user_ids_set = Set.fromList [initial_user_id]
        initial_art_tree     = ArtTree initial_bs_tree initial_user_ids_set

        t = do
          root_priv_key <- dhExchange (initialuser_info^?!userPrivateKey) (new_user_info^?!userPublicKey)
          root_pub_key  <- exponentiate root_priv_key
          let
            expected_user_ids_set = Set.fromList [initial_user_id, new_user_id]
            expected_tree         = Branch
                                    (leaf 0 (Right initialuser_info))
                                    (LBSInternalNode 2 1 $ Left $ InternalNodeInfo (Just root_priv_key) (Just root_pub_key))
                                    (leaf 1 (Right new_user_info))
          mt <- runMaybeT $ Hart.Tree.addUser new_user_id new_user_info (new_user_info^?!userPublicKey) initial_art_tree
          lift $ assertBool (fname ++ " " ++ "withLocalUser (treeData)") $ maybe False ((== expected_tree) . treeData) mt
          lift $ assertBool (fname ++ " " ++ "withLocalUser (treeUIDs)") $ maybe False ((== expected_user_ids_set) . treeUIDs) mt
      runHartTreeT t hartToStdErr (DHMaterial (0 :: Integer))

test_removeUser :: Test
test_removeUser = _HART_TREE_ ++ ".removeUser" ~: [ noLocalUser
                                                  , singleton
                                                  , twoUsers
                                                  ]
  where
    fname = "removeUser"
    twoUsersTest firstUserConstructor = do
      let
        (first_user_id, first_user_info)   = makeIdUserInfoPair 0 firstUserConstructor
        (second_user_id, second_user_info) = makeIdUserInfoPair 1 StartUpRemoteUser

        non_sensical_pk_value = MyDHComputableInteger 0
      root_priv_key <- dhExchange (first_user_info^?!userPrivateKey) (second_user_info^?!userPublicKey)
      root_pub_key  <- exponentiate root_priv_key
      let
        initial_user_ids_set = Set.fromList [first_user_id, second_user_id]
        initial_bs_tree      = Branch
                               (leaf 0 (Right first_user_info))
                               (LBSInternalNode 2 1 $ Left $ InternalNodeInfo (Just root_priv_key) (Just root_pub_key))
                               (leaf 1 (Right second_user_info))
        initial_art_tree     = ArtTree initial_bs_tree initial_user_ids_set
      runMaybeT $ Hart.Tree.removeUser second_user_id non_sensical_pk_value initial_art_tree
    noLocalUser = "testing for no local user" ~: TestCase $ do
      let t = do
            mt <- twoUsersTest StartUpRemoteUser
            lift $ assertBool (fname ++ " " ++ "noLocalUser") $ isNothing mt
      runHartTreeT t hartNoLogging (DHMaterial (0 :: Integer))
    singleton = "testing for singleton" ~: TestCase $ do
      let
        (sole_user_id, sole_user_info) = makeIdUserInfoPair 0 LocalUser
        non_sensical_pk_value          = MyDHComputableInteger 0
        initial_user_ids_set           = Set.fromList [sole_user_id]
        initial_bs_tree                = leaf 0 (Right sole_user_info)
        initial_art_tree               = ArtTree initial_bs_tree initial_user_ids_set
        t = do
          mt <- runMaybeT $ Hart.Tree.removeUser sole_user_id non_sensical_pk_value initial_art_tree
          lift $ assertBool (fname ++ " " ++ "singleton is nothing") $ isNothing mt
      runHartTreeT t hartToStdErr (DHMaterial (0 :: Integer))
    twoUsers = "testing for two users tree" ~: TestCase $ do
      let
        (first_user_id, first_user_info) = makeIdUserInfoPair 0 LocalUser
        expected_user_ids_set            = Set.fromList [first_user_id]
        expected_tree                    = leaf 0 (Right first_user_info)
        t = do
          mt <- twoUsersTest LocalUser
          lift $ assertBool (fname ++ " " ++ "twoUsers (treeData)") $ maybe False ((== expected_tree) . treeData) mt
          lift $ assertBool (fname ++ " " ++ "twoUsers (treeUIDs)") $ maybe False ((== expected_user_ids_set) . treeUIDs) mt
      runHartTreeT t hartToStdErr (DHMaterial (0 :: Integer))

test_updateKey :: Test
test_updateKey = _HART_TREE_ ++ ".updateKey" ~: [ noLocalUser
                                                , twoUsers
                                                , fourUsers
                                                ]
  where
    fname = "updateKey"
    noLocalUser = "testing for no local user" ~: TestCase $ do
      let
        (first_user_id, first_user_info)   = makeIdUserInfoPair 0 StartUpRemoteUser
        (second_user_id, second_user_info) = makeIdUserInfoPair 1 StartUpRemoteUser
        new_public_key = MyDHComputableInteger 2
        t = do
          root_priv_key <- dhExchange (first_user_info^?!userPrivateKey) (second_user_info^?!userPublicKey)
          root_pub_key  <- exponentiate root_priv_key
          let
            initial_user_ids_set = Set.fromList [first_user_id, second_user_id]
            initial_bs_tree      = Branch
                                     (leaf 0 (Right first_user_info))
                                     (LBSInternalNode 2 1 $ Left $ InternalNodeInfo (Just root_priv_key) (Just root_pub_key))
                                     (leaf 1 (Right second_user_info))
            initial_art_tree     = ArtTree initial_bs_tree initial_user_ids_set
          mt <- runMaybeT $ Hart.Tree.updateKey second_user_id new_public_key new_public_key initial_art_tree
          lift $ assertBool (fname ++ " " ++ "noLocalUser is nothing") $ isNothing mt
      runHartTreeT t hartNoLogging (DHMaterial (0 :: Integer))
    twoUsers = "testing for two users tree" ~: TestCase $ do
      let
        (first_user_id, first_user_info)   = makeIdUserInfoPair 0 LocalUser
        makeBSTree (second_user_id, second_user_info) = do
          root_priv_key <- dhExchange (first_user_info^?!userPrivateKey) (second_user_info^?!userPublicKey)
          root_pub_key  <- exponentiate root_priv_key
          let
            user_ids_set = Set.fromList [first_user_id, second_user_id]
            bs_tree      = Branch
                            (leaf 0 (Right first_user_info))
                            (LBSInternalNode 2 1 $ Left $ InternalNodeInfo (Just root_priv_key) (Just root_pub_key))
                            (leaf 1 (Right second_user_info))
          return $ ArtTree bs_tree user_ids_set
        t = do
          let
            second_user_data@(second_user_id, _) = makeIdUserInfoPair 1 StartUpRemoteUser
          initial_art_tree <- makeBSTree second_user_data

          let
            new_public_key            = MyDHComputableInteger 2
            (_, new_second_user_info) = makeIdUserInfoPair (getMyDHComputableInteger new_public_key) (\ _ b -> RemoteUser b)
          (ArtTree expected_tree expected_user_ids_set) <- makeBSTree (second_user_id, new_second_user_info)

          mt <- runMaybeT $ Hart.Tree.updateKey second_user_id new_public_key new_public_key initial_art_tree
          lift $ assertBool (fname ++ " " ++ "twoUsers (treeData)") $ maybe False ((== expected_tree) . treeData) mt
          lift $ assertBool (fname ++ " " ++ "twoUsers (treeUIDs)") $ maybe False ((== expected_user_ids_set) . treeUIDs) mt
      runHartTreeT t hartToStdErr (DHMaterial (0 :: Integer))
    fourUsers = "testing for four users tree" ~: TestCase $ do
      let
        (first_user_id, first_user_info)   = makeIdUserInfoPair 0 LocalUser
        (second_user_id, second_user_info) = makeIdUserInfoPair 1 StartUpRemoteUser
        (third_user_id, third_user_info)   = makeIdUserInfoPair 2 StartUpRemoteUser
        makeBSTree (fourth_user_id, fourth_user_info) = do
          left_sub_tree_vk  <- dhExchange (first_user_info^?!userPrivateKey) (second_user_info^?!userPublicKey)
          left_sub_tree_pk  <- exponentiate left_sub_tree_vk
          right_sub_tree_vk <- dhExchange (third_user_info^?!userPrivateKey) (fourth_user_info^?!userPublicKey)
          right_sub_tree_pk <- exponentiate right_sub_tree_vk
          root_vk           <- dhExchange left_sub_tree_vk right_sub_tree_pk
          root_pk           <- exponentiate root_vk
          let
            user_ids_set = Set.fromList [first_user_id, second_user_id, third_user_id, fourth_user_id]
            bs_tree      = Branch
                             (Branch
                               (leaf 0 (Right first_user_info))
                               (LBSInternalNode 2 1 $ Left $ InternalNodeInfo (Just left_sub_tree_vk) (Just left_sub_tree_pk))
                               (leaf 1 (Right second_user_info)))
                             (LBSInternalNode 4 2 $ Left $ InternalNodeInfo (Just root_vk) (Just root_pk))
                             (Branch
                               (leaf 2 (Right third_user_info))
                               (LBSInternalNode 2 1 $ Left $ InternalNodeInfo (Just right_sub_tree_vk) (Just right_sub_tree_pk))
                               (leaf 3 (Right fourth_user_info)))
          return (ArtTree bs_tree user_ids_set, right_sub_tree_pk)
        t = do
          let
            fourth_user_data@(fourth_user_id, _) = makeIdUserInfoPair 3 StartUpRemoteUser
          (initial_art_tree, _) <- makeBSTree fourth_user_data

          let
            new_public_key            = MyDHComputableInteger 4
            (_, new_fourth_user_info) = makeIdUserInfoPair (getMyDHComputableInteger new_public_key) (\ _ b -> RemoteUser b)
          (ArtTree expected_tree expected_user_ids_set, right_sub_tree_pk) <- makeBSTree (fourth_user_id, new_fourth_user_info)

          mt <- runMaybeT $ Hart.Tree.updateKey fourth_user_id new_public_key right_sub_tree_pk initial_art_tree
          lift $ assertBool (fname ++ " " ++ "fourUsers (treeData)") $ maybe False ((== expected_tree) . treeData) mt
          lift $ assertBool (fname ++ " " ++ "fourUsers (treeUIDs)") $ maybe False ((== expected_user_ids_set) . treeUIDs) mt
      runHartTreeT t hartToStdErr (DHMaterial (0 :: Integer))

--  vim: set sts=2 ts=2 sw=2 tw=120 et :

