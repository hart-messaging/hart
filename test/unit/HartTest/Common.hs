{-|
  Module      : Common
  Description : Common Hart testing package utilities.
  Copyright   : (c) Simon Désaulniers, 2021
  License     : GPL-3

  Common utilities for writing tests.
-}

module HartTest.Common where

import Hart.Internal.Data.LBSTree

leaf :: Int -> a -> LBSTree a
leaf ix = Leaf . LBSLeaf ix

--  vim: set sts=2 ts=2 sw=2 tw=120 et :

