
{-|
  Module      : HartTest.HUnit
  Description : Unit test utility module.
  Copyright   : (c) Simon Désaulniers, 2020
  License     : GPL-3

  Maintainer  : sim.desaulniers@gmail.com

  HUnit utility functions for handling logging and error cases while testing.
-}

{-# LANGUAGE FlexibleContexts #-}

module HartTest.HUnit ( runNothingAssertion
                      , runJustAssertion
                      , runChronicleAssertion
                      , runChronicleAssertionWithLog
                      , runChronicleAssertionWithLogStderr
                      ) where

import Data.Maybe

import Control.Monad
import Control.Monad.IO.Class
import Control.Monad.Trans.Maybe
import Control.Monad.Chronicle

import Colog.Monad
import Colog.Message

import Test.HUnit

import Hart.Internal.Log

{-| Unwraps the `MaybeT` transformer and process the assertion only if input was
   not `empty`.
-}
runJustAssertion :: MonadIO m => MaybeT m a -> m ()
runJustAssertion mbta = do
  ma <- runMaybeT mbta
  when (isNothing ma) $ liftIO $ assertFailure "An error occurred..."

{-| Opposite of `runJustAssertion`, i.e. runs the wrapped assertion only if
   input was `empty`.

   This is useful to suppress error messages when testing failing cases.
-}
runNothingAssertion :: MonadIO m => MaybeT m a -> m ()
runNothingAssertion mbta = do
  ma <- runMaybeT mbta
  when (isJust ma) $
    liftIO $ assertFailure "An error should have occurred, but no error was thrown..."

{-| Runs assertions ignoring any log from Chronicle monad.

   This can be useful for suppressing warning messages when testing functions
   which still succeed.
-}
runChronicleAssertion :: MonadIO m => ChronicleT Messages m a -> m ()
runChronicleAssertion = runJustAssertion . runChronicleWithoutLog

{-| Runs assertions in a Monad capable of logging messages.
-}
runChronicleAssertionWithLog :: (MonadIO m, WithLog env Message m) => ChronicleT Messages m a -> m ()
runChronicleAssertionWithLog = runJustAssertion . runChronicleWithLog

{-| Runs assertions with logging messages written on `stderr`.
-}
runChronicleAssertionWithLogStderr :: ChronicleT Messages (LoggerT Message IO) a -> IO ()
runChronicleAssertionWithLogStderr = runJustAssertion . runChronicleWithLogToStdErr

--  vim: set sts=2 ts=2 sw=2 tw=120 et :

