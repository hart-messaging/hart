
{-|
  Module      : LBSTree
  Description : Unit tests for module "Hart.Internal.Data.LBSTree".
  Copyright   : (c) Simon Désaulniers, 2020
  License     : GPL-3

  Maintainer  : sim.desaulniers@gmail.com
-}

module HartTest.Internal.Data.LBSTree (
  tests
) where

import Data.Maybe

import Control.Monad.Trans.Maybe
import Control.Monad.IO.Class

import Test.HUnit

---------------------
--  Local modules  --
---------------------

import Hart.Internal.MChronicle
import Hart.Internal.Data.LBSTree
import qualified Hart.Internal.Data.LBSTree as LBST

import HartTest.Common
import HartTest.HUnit

----------------
--  Bindings  --
----------------

_INTERNAL_DATA_LBSTREE_ :: String
_INTERNAL_DATA_LBSTREE_ = "Hart.Internal.Data.LBSTree"

tests :: [Test]
tests = [ test_path
        , test_copath
        , test_get
        , test_set
        , test_cpMapAccum
        , test_insert
        , test_fromList
        , test_delete
        , test_init
        , test_tail
        ]

basic_tree :: Int -> Int -> LBSTree Int
basic_tree x y = Branch (Leaf $ LBSLeaf 0 x) (LBSInternalNode 2 1 0) (Leaf $ LBSLeaf 1 y)

right_left_tree :: LBSTree Int
right_left_tree = Branch
                        (Branch
                          (Branch
                            (leaf 0 0)
                          (LBSInternalNode 2 1 0)
                            (leaf 1 1))
                      (LBSInternalNode 4 2 0)
                          (Branch
                            (leaf 2 2)
                          (LBSInternalNode 2 1 0)
                            (leaf 3 3)))
                    (LBSInternalNode 5 4 (0 :: Int))
                      (leaf 4 4)

-----------------
--  The tests  --
-----------------

test_path :: Test
test_path = _INTERNAL_DATA_LBSTREE_ ++ ".path" ~: runChronicleAssertionWithLogStderr $ do
  let n            = LBSLeaf 2 2
      expected_out = [ LBSInternalNode 5 4 0
                     , LBSInternalNode 4 2 0
                     , LBSInternalNode 2 1 0
                     , n
                     ]
  hoist' (LBST.path n right_left_tree)
    >>= liftIO . assertBool "path (LBSLeaf 2 2) right_left_tree" . (== expected_out)

test_copath :: Test
test_copath = _INTERNAL_DATA_LBSTREE_ ++ ".copath" ~: runChronicleAssertionWithLogStderr $ do
  let n            = LBSLeaf 2 2
      expected_out = [ LBSLeaf 4 4
                     , LBSInternalNode 2 1 0
                     , LBSLeaf 3 3
                     ]
  hoist' (LBST.copath n right_left_tree)
    >>= liftIO . assertBool "copath (LBSLeaf 2 2) right_left_tree" . (== expected_out)

test_get :: Test
test_get = _INTERNAL_DATA_LBSTREE_ ++ ".get" ~: [ basic
                                                , rightLeftPath
                                                ]
  where
    basic = "testing for basic 2-nodes tree" ~: runChronicleAssertionWithLogStderr $ do
      let t = basic_tree 3 5
          l = LBSLeaf 0 3
          r = LBSLeaf 1 5
      LBST.get 0 t >>= liftIO . assertBool "get 0 t" . (== l)
      LBST.get 1 t >>= liftIO . assertBool "get 1 t" . (== r)

    rightLeftPath =
      "testing for path that goes right, then left" ~: runChronicleAssertionWithLogStderr $ do
      let t            = right_left_tree
          expected_out = LBSLeaf 2 2
      hoist' (LBST.get 2 t) >>= liftIO . assertBool "get 2 t" . (== expected_out)

test_set :: Test
test_set = _INTERNAL_DATA_LBSTREE_ ++ ".set" ~: [ basic
                                                , rightLeftPath
                                                ]
  where
    basic = "testing for basic 2-nodes tree" ~: runChronicleAssertionWithLogStderr $ do
      let t            = basic_tree 2 3
          expected_out = Branch (leaf 0 2) (LBSInternalNode 2 1 (60 :: Int)) (leaf 1 30)
      LBST.set (\ x y -> return $ x * y) 30 1 t
        >>= liftIO . assertBool "set (*) 30" . (== expected_out)

    -- This path triggers the adjusting of the lookup key when looking in the
    -- right subtree. Therefore, the next id will be the subtraction of the left
    -- subtree size to the previous id just when we make the turn to the right
    -- subtree.
    rightLeftPath =
      "testing for path that goes right, then left" ~: runChronicleAssertionWithLogStderr $ do
      let t            = right_left_tree
          expected_out = Branch
                          (Branch
                            (Branch
                              (leaf 0 0)
                            (LBSInternalNode 2 1 2)
                              (leaf 1 2))
                          (LBSInternalNode 4 2 2)
                            (Branch
                              (leaf 2 2)
                            (LBSInternalNode 2 1 0)
                              (leaf 3 3)))
                        (LBSInternalNode 5 4 (6 :: Int))
                          (leaf 4 4)
      hoist' (LBST.set (\ x y -> return $ x + y) 2 1 t)
        >>= liftIO . assertBool "set 2 1" . (== expected_out)

test_cpMapAccum :: Test
test_cpMapAccum = _INTERNAL_DATA_LBSTREE_ ++ ".cpMapAccum" ~: [ identity
                                                              , rightLeftListDistribution
                                                              ]
  where
    identity = "testing for the \"identity\" function" ~: runChronicleAssertionWithLogStderr $ do
      -- Passing (,) is like mapping the identity function on the co-path nodes.
      let t            = basic_tree 2 3
          expected_out = t
      LBST.cpMapAccum (\ _ _ -> return 0) (,) 0 (0 :: Int) t
        >>= liftIO . assertBool "cpMapAccum const (,) 0 0 t" . (== expected_out)

    rightLeftListDistribution = "testing for a function which maps elements from a list"
                                ~: runChronicleAssertionWithLogStderr $ do
      let t              = right_left_tree
          -- only the 3 first numbers are used since the copath is of length 3
          mylist         = [2, 3, 5, 7, 11, 13, 17, 19] :: [Int]
          myMap (x:xs) _ = (xs, x)
          myMap [] v     = ([], v)
          expected_out   = Branch
                                (Branch
                                  (Branch
                                    (leaf 0 0)
                                  (LBSInternalNode 2 1 3)
                                    (leaf 1 1))
                              (LBSInternalNode 4 2 7)
                                  (Branch
                                    (leaf 2 2)
                                  (LBSInternalNode 2 1 4)
                                    (leaf 3 2)))
                            (LBSInternalNode 5 4 (12 :: Int))
                              (leaf 4 5)
      LBST.cpMapAccum (\ x y -> return $ x + y) myMap 2 mylist t
        >>= liftIO . assertBool "cpMapAccum (\\ x y -> return $ x + y) myMap 2 myList t" . (== expected_out)


test_insert :: Test
test_insert = _INTERNAL_DATA_LBSTREE_ ++ ".insert" ~: [ leafInsert
                                                      , insertTwice
                                                      , rightLeftInsert
                                                      ]
  where
    leafInsert = "testing for leaf as input" ~: runChronicleAssertionWithLogStderr $ do
      LBST.insert (\ x _ -> return x) 0 0 (leaf 0 (1 :: Int))
        >>= liftIO . assertBool "insert const 0 0 (leaf 0 1)" . (== basic_tree 0 (1 :: Int))

    insertTwice = "testing for appending already existing value" ~: assertion
      where
        assertion :: Assertion
        assertion = runChronicleAssertion $
          LBST.insert (\ x _ -> return x) 0 0 (leaf 0 (0 :: Int))
            >>= liftIO . assertBool "insert const 0 0 (leaf 0 0)" . (== leaf 0 (0 :: Int))

    rightLeftInsert =
      "testing for path that goes left, then right" ~: runChronicleAssertionWithLogStderr $ do
      let expected_out = Branch
                             (Branch
                               (Branch
                                 (leaf 0 0)
                               (LBSInternalNode 2 1 0)
                                 (leaf 1 1))
                           (LBSInternalNode 5 2 0)
                               (Branch
                                 (Branch
                                   (leaf 2 0)
                                 (LBSInternalNode 2 1 0)
                                   (leaf 3 2))
                               (LBSInternalNode 3 2 0)
                                 (leaf 4 3)))
                         (LBSInternalNode 6 5 (0 :: Int))
                           (leaf 5 4)
      hoist' (LBST.insert (\ x _ -> return x) 2 0 right_left_tree)
        >>= liftIO . assertBool "insert const 2 0" . (== expected_out)

test_fromList :: Test
test_fromList = _INTERNAL_DATA_LBSTREE_ ++ ".fromList" ~: [ empty
                                                          , singleton
                                                          , two
                                                          , three
                                                          ]
  where
    empty = "testing for empty list as input" ~: runChronicleAssertionWithLogStderr $
      hoist' (runMaybeT (LBST.fromList (\ x _ -> return x) ([] :: [Int])))
        >>= liftIO . assertBool "fromList []" . isNothing

    singleton = "testing for singleton list as input" ~: runChronicleAssertionWithLogStderr $ do
      mt <- hoist' $ runMaybeT $ LBST.fromList (\ x _ -> return x) ([0] :: [Int])
      liftIO $ do
        assertBool "fromList [0] should not be Nothing" $ isJust mt
        let t = fromJust mt
        assertBool "fromList [0]" $ leaf 0 0 == t

    two = "testing for 2-elements list as input" ~: runChronicleAssertionWithLogStderr $ do
      let expected_out = Branch (leaf 0 3) (LBSInternalNode 2 1 3) (leaf 1 2) :: LBSTree Int
      mt <- hoist' $ runMaybeT $ LBST.fromList (\ x _ -> return x) [3,2]
      liftIO $ do
        assertBool "fromList [3,2] should not be Nothing" $ isJust mt
        let t = fromJust mt
        assertBool "fromList [3,2]" $ expected_out == t

    three = "testing for 3-elements list as input" ~: runChronicleAssertionWithLogStderr $ do
      let expected_out = Branch
                          (Branch (leaf 0 4) (LBSInternalNode 2 1 7) (leaf 1 3))
                          (LBSInternalNode 3 2 (9 :: Int))
                          (leaf 2 2)
      mt <- hoist' $ runMaybeT (LBST.fromList (\ x y -> return $ x + y) [4,3,2])
      liftIO $ do
        assertBool "fromList [4,3,2] should not be Nothing" $ isJust mt
        let t = fromJust mt
        assertBool "fromList [4,3,2]" $ expected_out == t

test_delete :: Test
test_delete = _INTERNAL_DATA_LBSTREE_ ++ ".delete" ~: [ leafDelete
                                                      , two
                                                      , rightLeftDelete
                                                      ]
  where
    leafDelete = "testing for leaf as input" ~: runChronicleAssertionWithLogStderr $
      hoist' (runMaybeT (LBST.delete (\ x _ -> return x) 0 (leaf 0 0 :: LBSTree Int)))
        >>= liftIO . assertBool "delete (\\ x _ -> return x) 0 (Leaf 0)" . isNothing

    two = "testing for 2-elements tree as input" ~: runChronicleAssertionWithLogStderr $ do
      mt <- hoist' (runMaybeT (LBST.delete (\ x _ -> return x) 0 (basic_tree 0 1 :: LBSTree Int)))
      liftIO $ do
        assertBool "delete (\\ x _ -> return x) 0 (Branch (Leaf 0) _ (Leaf 1)) should not be Nothing" $ isJust mt
        let t = fromJust mt
        assertBool "delete (\\ x _ -> return x) 0 (Branch (Leaf 0) _ (Leaf 1))" $ leaf 0 1 == t

    rightLeftDelete = "testing for deletion down a right-left path" ~: runChronicleAssertionWithLogStderr $ do
      let expected_out = Branch
                             (Branch
                               (Branch
                                 (leaf 0 0)
                               (LBSInternalNode 2 1 0)
                                 (leaf 1 1))
                           (LBSInternalNode 3 2 0)
                               (leaf 2 3))
                         (LBSInternalNode 4 3 (0 :: Int))
                           (leaf 3 4)
      mt <- hoist' $ runMaybeT (LBST.delete (\ x _ -> return x) 2 right_left_tree)
      liftIO $ do
        assertBool "delete (\\ x _ -> return x) 2 right_left_tree should not be Nothing" $ isJust mt
        let t = fromJust mt
        assertBool "delete (\\ x _ -> return x) 2 right_left_tree" $ expected_out == t

test_init :: Test
test_init = _INTERNAL_DATA_LBSTREE_ ++ ".init" ~: [ leafTail
                                                  , two
                                                  , three
                                                  ]

  where
    leafTail = "testing for leaf as input" ~: runChronicleAssertionWithLogStderr $
      hoist' (runMaybeT (LBST.init (\ x _ -> return x) (leaf 0 0 :: LBSTree Int)))
        >>= liftIO . assertBool "init (\\ x _ -> return x) (Leaf 0) should be Nothing" . isNothing

    two = "testing for 2-elements tree as input" ~: runChronicleAssertionWithLogStderr $ do
      mt <- hoist' $ runMaybeT (LBST.init (\ x _ -> return x) (basic_tree 0 1 :: LBSTree Int))
      liftIO $ do
        assertBool "init (\\ x _ -> return x) (Branch (Leaf 0) _ (Leaf 1)) should not be Nothing" $ isJust mt
        let t = fromJust mt
        assertBool "init (\\ x _ -> return x) (Branch (Leaf 0) _ (Leaf 1))" $ leaf 0 0 == t

    three = "testing for 3-elements tree as input" ~: runChronicleAssertionWithLogStderr $ do
      let t = Branch
              (Branch (leaf 0 0) (LBSInternalNode 2 1 0) (leaf 1 1))
              (LBSInternalNode 3 2 (0 :: Int))
              (leaf 2 2)
          expected_out = Branch (leaf 0 0) (LBSInternalNode 2 1 0) (leaf 1 1)
      mout <- hoist' $ runMaybeT $ LBST.init (\ x y -> return $ x + y) t
      liftIO $ do
        assertBool "init (+) tree should not be Nothing" $ isJust mout
        let out = fromJust mout
        assertBool "init (+) tree" $ expected_out == out

test_tail :: Test
test_tail = _INTERNAL_DATA_LBSTREE_ ++ ".tail" ~: [ leafTail
                                                  , two
                                                  , three
                                                  ]
  where
    leafTail = "testing for leaf as input" ~: runChronicleAssertionWithLogStderr $
      hoist' (runMaybeT (LBST.tail (\ x _ -> return x) (leaf 0 0 :: LBSTree Int)))
        >>= liftIO . assertBool "tail (\\ x _ -> return x) (Leaf 0) should be Nothing" . isNothing

    two = "testing for 2-elements tree as input" ~: runChronicleAssertionWithLogStderr $ do
      mt <- hoist' $ runMaybeT (LBST.tail (\ x _ -> return x) (basic_tree 0 1 :: LBSTree Int))
      liftIO $ do
        assertBool "tail (\\ x _ -> return x) (Branch (Leaf 0) _ (Leaf 1)) should not be Nothing" $ isJust mt
        let t = fromJust mt
        assertBool "tail (\\ x _ -> return x) (Branch (Leaf 0) _ (Leaf 1))" $ leaf 0 1 == t

    three = "testing for 3-elements tree as input" ~: runChronicleAssertionWithLogStderr $ do
      let t = Branch
              (Branch (leaf 0 0) (LBSInternalNode 2 1 0) (leaf 1 1))
              (LBSInternalNode 3 2 (0 :: Int))
              (leaf 2 2)
          expected_out = Branch (leaf 0 1) (LBSInternalNode 2 1 (3 :: Int)) (leaf 1 2)
      mout <- hoist' $ runMaybeT $ LBST.tail (\ x y -> return $ x + y) t
      liftIO $ do
        assertBool "tail (+) tree should not be Nothing" $ isJust mout
        let out = fromJust mout
        assertBool "tail (+) tree" $ expected_out == out

--  vim: set sts=2 ts=2 sw=2 tw=120 et :

