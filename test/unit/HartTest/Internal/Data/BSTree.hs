
{-|
  Module      : HartTest.Internal.Data.BSTree
  Description : Unit tests for module "Hart.Internal.Data.BSTree"
  Copyright   : (c) Simon Désaulniers, 2020
  License     : GPL-3

  Maintainer  : sim.desaulniers@gmail.com

  Unit testing for the "Hart.Internal.Data.BSTree" module.
-}

{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module HartTest.Internal.Data.BSTree (
  tests
) where

import Control.Monad.Trans

import Test.HUnit

---------------------
--  Local modules  --
---------------------

import HartTest.HUnit
import Hart.Internal.MChronicle

import Hart.Internal.Log
import Hart.Internal.Data.BSTree
import qualified Hart.Internal.Data.BSTree as BST

newtype Unit = Unit Int
  deriving (Num, Ord, Eq, Show)

instance DynamicallyIndexable Unit where
  dindex i _ _ = i

----------------
--  Bindings  --
----------------

_INTERNAL_DATA_BSTREE_ :: String
_INTERNAL_DATA_BSTREE_ = "Hart.Internal.Data.BSTree"

tests :: [Test]
tests = [ test_root
        , test_catamorph
        , test_mapL
        , test_internals
        , test_leaves
        , test_path
        , test_copath
        ]

basic_tree :: BSTree Unit
basic_tree = Branch (Leaf 0) 1 (Leaf 2)

-----------------
--  The tests  --
-----------------

test_root :: Test
test_root = _INTERNAL_DATA_BSTREE_ ++ ".root" ~: TestList $ [ leaf
                                                            , branch
                                                            ]
  where
    leaf = "testing for input: Leaf 0" ~: do
      let r = root $ Leaf (0 :: Unit)
      assertEqual "root of (Leaf 0)" 0 r

    branch = "testing for input: Branch (Leaf 0) 1 (Leaf 2)" ~: do
      let r = root basic_tree
      assertEqual "root of (Branch _ 1 _)" 1 r

test_catamorph :: Test
test_catamorph = _INTERNAL_DATA_BSTREE_ ++ ".catamorph" ~: TestList $ [ leaf
                                                                      , branch
                                                                      ]
  where
    append v acc          = v : acc
    subconcat v acc0 acc1 = acc0 ++ [v] ++ acc1

    leaf = "testing for input: Leaf" ~: do
      let
        v = 1542 :: Unit
        c = catamorph (append, subconcat) [] (Leaf v)
      assertEqual
        ("catamorph (append, subconcat) [] (Leaf "++show v++") should be []") c [v]

    branch = "testing for input: Branch" ~: do
      let
        r = 9852 :: Unit
        c = catamorph (append, subconcat) [] (Branch (Leaf 0) r (Leaf 1))
      assertEqual ("catamorph (append, subconcat) [] (Branch (Leaf 0) "++show r++" (Leaf 1))")
        [0,r,1] c

test_mapL :: Test
test_mapL = _INTERNAL_DATA_BSTREE_ ++ ".mapL" ~: TestList $ [ leaf
                                                            , branch
                                                            ]
  where
    leaf = "testing for input: Leaf 5420" ~: do
      let
        v = 5420 :: Unit
        out = mapL (+1) $ Leaf v
      assertBool
        ("mapL (+1) (Leaf "++show v++") should be (Leaf "++show (v+1)++")") $ Leaf (v+1) == out

    branch = "testing for input: Branch (Leaf 0) 1942 (Leaf 1)" ~: do
      let
        v   = 1942 :: Unit
        out = mapL (flip (-) 1) $ Branch (Leaf 0) v (Leaf 1)
      assertBool
        ("mapL (flip (-) 1) (Branch (Leaf 0) "++show v++" (Leaf 1))\
         \ should be (Branch (Leaf -1) "++show v++" (Leaf 0))") $
        Branch (Leaf (-1)) v (Leaf 0) == out

test_internals :: Test
test_internals = _INTERNAL_DATA_BSTREE_ ++ ".internals" ~: TestList $ [ leaf
                                                                      , branch
                                                                      ]
  where
    leaf = "testing for input: Leaf 4892" ~: do
      let
        v   = 4892 :: Unit
        out = internals (Leaf v)
      assertEqual ("internals (Leaf "++show v++")") [] out

    branch = "testing for input: Branch (Leaf 0) 2381 (Leaf 2)" ~: do
      let
        v   = 2381 :: Unit
        out = internals $ Branch (Leaf 0) v (Leaf 2)
      assertEqual ("internals (Branch (Leaf 0) "++show v++" (Leaf 2))") [v] out

test_leaves :: Test
test_leaves = _INTERNAL_DATA_BSTREE_ ++ ".leaves" ~: TestList $ [ leaf
                                                                , branch
                                                                ]
  where
    leaf = "testing for input: Leaf 2188" ~: do
      let
        v   = 2188 :: Unit
        out = leaves (Leaf v)
      assertEqual ("internals (Leaf "++show v++")") [v] out

    branch = "testing for input: Branch (Leaf 0) 3492 (Leaf 2)" ~: do
      let
        v   = 3492 :: Unit
        out = leaves $ Branch (Leaf 0) v (Leaf 2)
      assertEqual ("internals (Branch (Leaf 0) "++show v++" (Leaf 2))") [0, 2] out

test_path :: Test
test_path = _INTERNAL_DATA_BSTREE_ ++ ".path" ~: TestList [ leaf
                                                          , branch
                                                          , unknownNode
                                                          ]
  where
    leaf = "testing for input: Leaf 6519" ~: runChronicleAssertionWithLogStderr $ do
      let v   = 6519 :: Unit
          msg = "path "++show v++" (Leaf "++show v++")"
      hoist' (BST.path v (Leaf v))
        >>= liftIO . assertEqual msg [v]

    branch =
      "testing for input: Branch (Leaf 0) 1 (Leaf 2)" ~: runChronicleAssertionWithLogStderr $ do
      hoist' (BST.path 0 basic_tree)
        >>= liftIO . assertEqual "path 0 (Branch (Leaf 0) 1 (Leaf 2)" [1, 0]
      hoist' (BST.path 2 basic_tree)
        >>= liftIO . assertEqual "path 2 (Branch (Leaf 0) 1 (Leaf 2)" [1, 2]

    unknownNode = "testing for \"unkown node\" error" ~: unknownNodeError
      where
        p = BST.path 1 basic_tree
        unknownNodeError :: Assertion
        unknownNodeError = runNothingAssertion . runChronicleWithoutLog $ hoist' p

test_copath :: Test
test_copath = _INTERNAL_DATA_BSTREE_ ++ ".copath" ~: TestList [ leaf
                                                              , branch
                                                              , unknownNode
                                                              ]
  where
    leaf = "testing for input: Leaf 4283" ~: runChronicleAssertionWithLogStderr $ do
      let v = 4283 :: Unit
      hoist' (BST.copath v (Leaf v))
        >>= liftIO . assertEqual ("copath "++show v++" (Leaf "++show v++")") []

    branch =
      "testing for input: Branch (Leaf 0) 1 (Leaf 2)" ~: runChronicleAssertionWithLogStderr $ do
      hoist' (BST.copath 0 basic_tree)
        >>= liftIO . assertEqual "copath 0 Branch (Leaf 0) 1 (Leaf 2)" [2]
      hoist' (BST.copath 2 basic_tree)
        >>= liftIO . assertEqual "copath 2 Branch (Leaf 0) 1 (Leaf 2)" [0]

    unknownNode = "testing for \"unkown node\" error" ~: unknownNodeError
      where
        cp = BST.copath 1 basic_tree
        unknownNodeError :: Assertion
        unknownNodeError = runNothingAssertion . runChronicleWithoutLog $ hoist' cp

--  vim: set sts=2 ts=2 sw=2 tw=120 et :

