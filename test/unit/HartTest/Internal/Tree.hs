
{-| Unit testing for the "Hart.Internal.Tree" module.
-}
module HartTest.Internal.Tree (
  tests
) where

import Test.HUnit

_INTERNAL_TREE_ :: String
_INTERNAL_TREE_ = "Hart.Internal.Tree"

tests :: [Test]
tests = []

--  vim: set sts=2 ts=2 sw=2 tw=120 et :

